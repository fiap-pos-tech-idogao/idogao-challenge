FROM maven:3.9.6-eclipse-temurin-22 as builder

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN mvn package

FROM openjdk:22-ea-21-slim-bullseye

COPY --from=builder /usr/src/app/target/*.jar /app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app.jar"]
