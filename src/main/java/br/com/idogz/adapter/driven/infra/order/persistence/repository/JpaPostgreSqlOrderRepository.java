package br.com.idogz.adapter.driven.infra.order.persistence.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.idogz.adapter.driven.infra.order.persistence.entity.OrderEntity;
import br.com.idogz.adapter.driven.infra.order.persistence.entity.OrderView;

public interface JpaPostgreSqlOrderRepository extends JpaRepository<OrderEntity, Integer>, JpaSpecificationExecutor<OrderEntity> {

	@Query(value = "select * FROM public.create_order(:_dateorder, :_totalvalueorder, :_idstatus, :_idclient, :_notesorder);", nativeQuery = true)
	OrderView save(@Param("_dateorder") LocalDate date,
			@Param("_totalvalueorder") Double totalvalueorder, @Param("_idstatus") Integer idstatus,
			@Param("_idclient") Integer idclient, @Param("_notesorder") String notesorder);

}
