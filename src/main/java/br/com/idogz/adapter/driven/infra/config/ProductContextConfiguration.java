package br.com.idogz.adapter.driven.infra.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.idogz.adapter.driven.infra.product.persistence.ProductRepositoryImpl;
import br.com.idogz.adapter.driven.infra.product.persistence.repository.JpaPostgreSqlProductRepository;
import br.com.idogz.core.application.factory.product.ProductFactory;
import br.com.idogz.core.application.factory.product.ProductFactoryImpl;
import br.com.idogz.core.application.ports.product.ProductRepository;
import br.com.idogz.core.application.ports.category.CategoryRepository;
import br.com.idogz.core.application.ports.product.ProductService;
import br.com.idogz.core.application.services.product.ProductServiceImpl;

@Configuration
public class ProductContextConfiguration {

	@Bean
	ProductService productService(ProductRepository repository) {
		return new ProductServiceImpl(repository);
	}
	
	@Bean
	ProductRepository productRepository(JpaPostgreSqlProductRepository repository, ProductFactory factory) {
		return new ProductRepositoryImpl(repository, factory);
	}
	
	@Bean
	ProductFactory productFactory(final CategoryRepository categoryRepository) {
		return new ProductFactoryImpl(categoryRepository);
	}
	
}
