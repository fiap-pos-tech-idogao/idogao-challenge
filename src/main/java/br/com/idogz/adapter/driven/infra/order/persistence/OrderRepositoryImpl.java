package br.com.idogz.adapter.driven.infra.order.persistence;

import java.util.ArrayList;
import java.util.List;

import br.com.idogz.adapter.driven.infra.category.persistence.entity.CategoryEntity;
import br.com.idogz.adapter.driven.infra.order.persistence.entity.OrderEntity;
import br.com.idogz.adapter.driven.infra.order.persistence.entity.OrderProductEntity;
import br.com.idogz.adapter.driven.infra.order.persistence.entity.OrderView;
import br.com.idogz.adapter.driven.infra.order.persistence.exception.OrderIdNotFoundException;
import br.com.idogz.adapter.driven.infra.order.persistence.repository.JpaPostgreSqlOrderRepository;
import br.com.idogz.adapter.driven.infra.order.persistence.specification.OrderSpecifications;
import br.com.idogz.adapter.driven.infra.product.persistence.entity.ProductEntity;
import br.com.idogz.core.application.factory.order.OrderBuilder;
import br.com.idogz.core.application.ports.order.OrderRepository;
import br.com.idogz.core.domain.Order;
import br.com.idogz.core.domain.exception.DomainException;

public class OrderRepositoryImpl implements OrderRepository {

	private final JpaPostgreSqlOrderRepository orderRepository;
	private final OrderBuilder builder;

	public OrderRepositoryImpl(final JpaPostgreSqlOrderRepository orderRepository, final OrderBuilder builder) {
		this.orderRepository = orderRepository;
		this.builder = builder;
	}

	@Override
	public void add(final Order order) throws DomainException {
		final var savedOrderEntity = save(order);
		order.updateIdWtihNotZero(savedOrderEntity.getIdOrder());
		order.updateTicket(savedOrderEntity.getTicketOrder());
	}

	@Override
	public List<Order> allOrders() throws DomainException {
		final var orders = new ArrayList<Order>();
		final var orderEntities = orderRepository.findAll(OrderSpecifications.all());
		for (final OrderEntity orderEntity : orderEntities) {
			final var order = toOrder(orderEntity);
			orders.add(order);
		}
		return orders;
	}

	@Override
	public Order orderWithId(final int id) throws DomainException {
		final var orderEntity = orderRepository.findById(id).orElseThrow(() -> new OrderIdNotFoundException(id));
		return toOrder(orderEntity);
	}

	private OrderView save(final Order order) {
		return orderRepository.save(order.date(), order.totalValue().doubleValue(),
				order.status().getId(), order.client().id(), order.notes().getValue());
	}

	private Order toOrder(final OrderEntity entity) throws DomainException {
		builder.idOrder(entity.getIdOrder());
		builder.client(entity.getClient().id());
		builder.ticketOrder(entity.getTicketOrder());
		builder.date(entity.getDateOrder());
		builder.notes(entity.getNotesOrder());
		builder.status(entity.getStatus().getId());
		addProducts(entity);
		return builder.build();
	}

	private void addProducts(final OrderEntity entity) throws DomainException {
		for (final OrderProductEntity orderProductEntity : entity.getOrderProducts()) {
			final int productId = getProductId(orderProductEntity);
			final var productName = getProductName(orderProductEntity);
			final int categoryId = getProductCategoryId(orderProductEntity);
			final var categoryName = getProductCategoryName(orderProductEntity);
			final var price = getProductPrice(orderProductEntity);
			final var description = getProductDescription(orderProductEntity);
			final var active = getProductActive(orderProductEntity);
			final var urlImage = getProductUrlImage(orderProductEntity);
			final int quantity = getProductOrderQuantity(orderProductEntity);
			final var notes = getProductOrderNotes(orderProductEntity);
			builder.addProduct(categoryId, categoryName, productName, price, description, active, urlImage, quantity, notes, productId);
		}
	}

	private CategoryEntity getProductCategory(final ProductEntity product) {
		return product.category();
	}

	private ProductEntity getProduct(final OrderProductEntity orderProductEntity) {
		return orderProductEntity.getId().getProduct();
	}

	private Integer getProductId(final OrderProductEntity orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		return product.id();
	}

	private String getProductName(final OrderProductEntity orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		return product.name();
	}

	private String getProductCategoryName(final OrderProductEntity orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		final var categoryEntity = getProductCategory(product);
		return categoryEntity.name();
	}

	private Integer getProductCategoryId(final OrderProductEntity orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		final var categoryEntity = getProductCategory(product);
		return categoryEntity.id();
	}

	private Double getProductPrice(final OrderProductEntity orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		return product.price();
	}

	private String getProductDescription(final OrderProductEntity orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		return product.description();
	}

	private Boolean getProductActive(final OrderProductEntity orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		return product.active();
	}

	private String getProductUrlImage(final OrderProductEntity orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		return product.urlImage();
	}

	private Integer getProductOrderQuantity(final OrderProductEntity orderProductEntity) {
		return orderProductEntity.getQuantity();
	}

	private String getProductOrderNotes(final OrderProductEntity orderProductEntity) {
		return orderProductEntity.getNotes();
	}

}
