package br.com.idogz.adapter.driven.infra.order.persistence.entity;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "tbOrderProduct", schema = "public")
public class OrderProductEntity {

	@EmbeddedId
	private IdOrderProduct id;

	@Column(nullable = false)
	private Integer quantity;

	@Column(length = 200)
	private String notes;

	public OrderProductEntity() {
	}

	public OrderProductEntity(final IdOrderProduct id, final Integer quantity, final String notes) {
		this.id = id;
		this.quantity = quantity;
		this.notes = notes;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(final Integer quantity) {
		this.quantity = quantity;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(final String notes) {
		this.notes = notes;
	}

	public IdOrderProduct getId() {
		return id;
	}

	public void setId(final IdOrderProduct id) {
		this.id = id;
	}

	public int getIdProduct() {
		return id.getProduct().id();
	}

	public int getIdOrder() {
		return id.getOrder().getIdOrder();
	}

}
