package br.com.idogz.adapter.driven.infra.config;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.PlatformTransactionManager;

import br.com.idogz.adapter.driven.infra.TransactionManagerHelperImpl;
import br.com.idogz.adapter.driven.infra.order.persistence.OrderProductRepositoryImpl;
import br.com.idogz.adapter.driven.infra.order.persistence.OrderRepositoryImpl;
import br.com.idogz.adapter.driven.infra.order.persistence.repository.JpaPostgreSqlOrderProductRepository;
import br.com.idogz.adapter.driven.infra.order.persistence.repository.JpaPostgreSqlOrderRepository;
import br.com.idogz.core.application.factory.order.OrderBuilder;
import br.com.idogz.core.application.factory.order.OrderBuilderImpl;
import br.com.idogz.core.application.factory.order.OrderProductFactory;
import br.com.idogz.core.application.factory.order.OrderProductFactoryImpl;
import br.com.idogz.core.application.ports.client.ClientRepository;
import br.com.idogz.core.application.ports.order.OrderProductRepository;
import br.com.idogz.core.application.ports.order.OrderRepository;
import br.com.idogz.core.application.ports.order.OrderService;
import br.com.idogz.core.application.ports.product.ProductRepository;
import br.com.idogz.core.application.ports.transaction.TransactionManagerHelper;
import br.com.idogz.core.application.services.order.OrderServiceImpl;

@Configuration
public class OrderContextConfiguration {

	@Bean
	@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	TransactionManagerHelper transactionManagerHelper(final PlatformTransactionManager transactionManager) {
		return new TransactionManagerHelperImpl(transactionManager);
	}

	@Bean
	OrderProductFactory orderProductFactory(final ProductRepository productRepositoy) {
		return new OrderProductFactoryImpl(productRepositoy);
	}

	@Bean
	OrderBuilder orderBuilder(final ClientRepository clientRepository, final OrderProductFactory orderProductFactory) {
		return new OrderBuilderImpl(clientRepository, orderProductFactory);
	}

	@Bean
	OrderRepository orderRepository(final JpaPostgreSqlOrderRepository orderRepository, final OrderBuilder builder) {
		return new OrderRepositoryImpl(orderRepository, builder);
	}

	@Bean
	OrderProductRepository orderProductRepository(final JpaPostgreSqlOrderProductRepository orderProductRepository) {
		return new OrderProductRepositoryImpl(orderProductRepository);
	}

	@Bean
	OrderService orderService(final OrderRepository orderRepository,
			final OrderProductRepository orderProductRepository, final TransactionManagerHelper transactionManagerHelper) {
		return new OrderServiceImpl(orderRepository, orderProductRepository, transactionManagerHelper);
	}

}
