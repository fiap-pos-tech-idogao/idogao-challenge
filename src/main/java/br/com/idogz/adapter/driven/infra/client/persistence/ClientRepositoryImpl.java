package br.com.idogz.adapter.driven.infra.client.persistence;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.idogz.adapter.driven.infra.client.persistence.entity.ClientEntity;
import br.com.idogz.adapter.driven.infra.client.persistence.exception.ClientCpfNotFoundException;
import br.com.idogz.adapter.driven.infra.client.persistence.exception.ClientIdNotFoundException;
import br.com.idogz.adapter.driven.infra.client.persistence.exception.DuplicatedCpfException;
import br.com.idogz.adapter.driven.infra.client.persistence.repository.ClientJpaPostgreSqlRepository;
import br.com.idogz.core.application.factory.client.ClientFactory;
import br.com.idogz.core.application.ports.client.ClientRepository;
import br.com.idogz.core.domain.Client;
import br.com.idogz.core.domain.Cpf;
import br.com.idogz.core.domain.exception.DomainException;

public class ClientRepositoryImpl implements ClientRepository {

	private static final String ERROR_DUPLICATE_KEY_VALUE_VIOLATES_UNIQUE_CONSTRAINT = "ERROR: duplicate key value violates unique constraint";
	private final ClientJpaPostgreSqlRepository repository;
	private final ClientFactory factory;

	public ClientRepositoryImpl(final ClientJpaPostgreSqlRepository repository, final ClientFactory factory) {
		this.repository = repository;
		this.factory = factory;
	}

	@Override
	public Client create(final Client client) throws DomainException {
		final var entity = ClientRepositoryImpl.toEntity(client);
		return save(entity);
	}

	private Client save(final ClientEntity entity) throws DomainException {
		try {
			final var savedEntity = repository.save(entity);
			return toClient(savedEntity);
		} catch (final DataIntegrityViolationException e) {
			throw ClientRepositoryImpl.handlerException(entity, e);
		}
	}

	@Override
	public Client find(final Cpf cpf) throws DomainException {
		final var clientEntity = repository.getClientByCpf(cpf.unformattingValue())
				.orElseThrow(() -> new ClientCpfNotFoundException(cpf));
		return toClient(clientEntity);
	}

	private static ClientEntity toEntity(final Client client) {
		return new ClientEntity(client.id(), client.name(), client.unformattingCpf(), client.emailAddress());
	}

	private Client toClient(final ClientEntity entity) throws DomainException {
		return factory.create(entity.id(), entity.name(), entity.cpf(), entity.email());
	}

	private static DomainException handlerException(final ClientEntity entity, final DataIntegrityViolationException e)
			throws DomainException {
		final var message = e.getMostSpecificCause().getMessage();
		if (message.contains(ERROR_DUPLICATE_KEY_VALUE_VIOLATES_UNIQUE_CONSTRAINT)) {
			return new DuplicatedCpfException(new Cpf(entity.cpf()));
		}
		return new DomainException(message);
	}

	@Override
	public Client find(final int id) throws DomainException {
		final var clientEntity = repository.findById(id).orElseThrow(() -> new ClientIdNotFoundException(id));
		return toClient(clientEntity);
	}

}
