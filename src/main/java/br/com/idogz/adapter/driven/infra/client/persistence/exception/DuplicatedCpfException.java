package br.com.idogz.adapter.driven.infra.client.persistence.exception;

import java.text.MessageFormat;

import br.com.idogz.core.domain.Cpf;
import br.com.idogz.core.domain.exception.DomainException;

public class DuplicatedCpfException extends DomainException {

	private static final String CPF_ALREADY_EXISTS = "CPF already exists: {0}";
	/**
	 *
	 */
	private static final long serialVersionUID = 9189556126034136467L;

	public DuplicatedCpfException(final Cpf cpf) {
		super(MessageFormat.format(DuplicatedCpfException.CPF_ALREADY_EXISTS, cpf.value()));
	}

}
