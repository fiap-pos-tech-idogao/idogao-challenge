package br.com.idogz.adapter.driven.infra.product.persistence.exception;

import br.com.idogz.core.domain.exception.DomainException;

public class ProductNameExistingException extends DomainException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6232136759238965693L;

	public ProductNameExistingException(String name) {
		super("Product with name " + name + " already exists");
	}
}
