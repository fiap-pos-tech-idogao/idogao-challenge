package br.com.idogz.adapter.driven.infra.product.persistence;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.idogz.adapter.driven.infra.category.persistence.entity.CategoryEntity;
import br.com.idogz.adapter.driven.infra.product.persistence.entity.ProductEntity;
import br.com.idogz.adapter.driven.infra.category.persistence.exception.CategoryIdNotFoundException;
import br.com.idogz.adapter.driven.infra.product.persistence.exception.DuplicatedNameException;
import br.com.idogz.adapter.driven.infra.product.persistence.exception.ProductIdNotFoundException;
import br.com.idogz.adapter.driven.infra.product.persistence.exception.ProductNameExistingException;
import br.com.idogz.adapter.driven.infra.product.persistence.exception.ProductNameNotFoundException;
import br.com.idogz.adapter.driven.infra.product.persistence.repository.JpaPostgreSqlProductRepository;
import br.com.idogz.core.application.factory.product.ProductFactory;
import br.com.idogz.core.application.ports.product.ProductRepository;
import br.com.idogz.core.domain.Product;
import br.com.idogz.core.domain.Name;
import br.com.idogz.core.domain.exception.DomainException;

public class ProductRepositoryImpl implements ProductRepository {

	private final JpaPostgreSqlProductRepository repository;
	private final ProductFactory factory;

	public ProductRepositoryImpl(final JpaPostgreSqlProductRepository repository, final ProductFactory factory) {
		this.repository = repository;
		this.factory = factory;
	}
	
	@Override
	public Product create(final Product client) throws DomainException {
		final var entity = toEntity(client);
		return persistenceSave(entity);
	}

	@Override
	public void update(final Product product) throws DomainException {
		final var entity = toEntity(product);
		persistenceUpdate(entity);
	}

	@Override
	public void remove(final Integer productId) throws DomainException {
		final var productEntity = repository.getProductById(productId).orElseThrow(() -> new ProductIdNotFoundException(productId));
		productEntity.setActive(false);
		persistenceRemove(productEntity);
	}
	
	@Override
	public Product findById(final Integer idProduct) throws DomainException {
		final var productEntity = repository.getProductById(idProduct).orElseThrow(() -> new ProductIdNotFoundException(idProduct));
		return toProduct(productEntity);
	}

	@Override
	public List<Product> findByCategoryId(final Integer idCategory) throws DomainException {
		final var productEntity = repository.getProductByCategoryId(idCategory).orElseThrow(() -> new CategoryIdNotFoundException(idCategory));
		return toListProduct(productEntity);
	}

	@Override
	public Product findByName(final Name name) throws DomainException {
		final var productEntity = repository.getProductByName(name.value()).orElseThrow(() -> new ProductNameNotFoundException(name));
		return toProduct(productEntity);
	}

	private Product persistenceSave(final ProductEntity entity) throws DomainException {
		try {
			this.existsProductName(entity);

			final var savedEntity = repository.save(entity);
			return toProduct(savedEntity);
		} catch (final DataIntegrityViolationException e) {
			throw handlerException(entity, e);
		}
	}

	private Product persistenceUpdate(final ProductEntity entity) throws DomainException {
		try {
			this.existsProductName(entity);

			final var updatedEntity = repository.save(entity);
			return toProduct(updatedEntity);
		} catch (final DataIntegrityViolationException e) {
			throw handlerException(entity, e);
		}
	}

	private Product persistenceRemove(final ProductEntity entity) throws DomainException {
		try {
			final var updatedEntity = repository.save(entity);
			return toProduct(updatedEntity);
		} catch (final DataIntegrityViolationException e) {
			throw handlerException(entity, e);
		}
	}

	private ProductEntity toEntity(final Product product) {
		return new ProductEntity(product.id(), new CategoryEntity(product.category().id(), product.category().name()), product.name(), product.price(), product.description(), product.active(), product.urlImage());
	}

	private Product toProduct(final ProductEntity entity) throws DomainException {
		return factory.create(entity.id(), entity.category().id(), entity.name(), entity.price(), entity.description(), entity.active(), entity.urlImage());
	}

	private List<Product> toListProduct(final List<ProductEntity> listEntity) throws DomainException {
		List<Product> result = new ArrayList<Product>();
		for (ProductEntity productEntity : listEntity) {
			result.add(toProduct(productEntity));
		}
		return result;
	}

	private void existsProductName(final ProductEntity entity) throws DomainException {
		final Optional<ProductEntity> productEntity = repository.getProductByName(entity.name());
		//If the name exists and is different from the current product, it raises an exception
		if(productEntity.isPresent() && entity.id() != productEntity.get().id())
			throw new ProductNameExistingException(entity.name());
	}

	private DomainException handlerException(final ProductEntity entity, final DataIntegrityViolationException e) throws DomainException {
		final var message = e.getMostSpecificCause().getMessage();
		if (message.contains("ERROR: duplicate key value violates unique constraint")) {
			return new DuplicatedNameException(new Name(entity.name()));
		}
		return new DomainException(message);
	}

}
