package br.com.idogz.adapter.driven.infra.category.persistence.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.idogz.adapter.driven.infra.category.persistence.entity.CategoryEntity;


public interface JpaPostgreSqlCategoryRepository extends JpaRepository<CategoryEntity, Integer> {

	@Query(value = "select * from get_category_by_id(:_categoryid)", nativeQuery = true)
	Optional<CategoryEntity> getCategoryById(@Param("_categoryid") Integer categoryId);

}
