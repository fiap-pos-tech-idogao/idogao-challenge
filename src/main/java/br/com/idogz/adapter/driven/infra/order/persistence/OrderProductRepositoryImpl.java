package br.com.idogz.adapter.driven.infra.order.persistence;

import java.util.List;

import br.com.idogz.adapter.driven.infra.order.persistence.repository.JpaPostgreSqlOrderProductRepository;
import br.com.idogz.core.application.ports.order.OrderProductRepository;
import br.com.idogz.core.domain.OrderProduct;
import br.com.idogz.core.domain.exception.DomainException;

public class OrderProductRepositoryImpl implements OrderProductRepository {

	private final JpaPostgreSqlOrderProductRepository orderProductRepository;

	public OrderProductRepositoryImpl(final JpaPostgreSqlOrderProductRepository orderProductRepository) {
		this.orderProductRepository = orderProductRepository;
	}

	@Override
	public void addAll(final List<OrderProduct> orderProducts) throws DomainException {
		orderProducts.forEach((final OrderProduct orderProduct) -> orderProductRepository.saveOrderProduct(orderProduct.idOrder(), orderProduct.idProduct(), orderProduct.quantity(), orderProduct.notes()));
	}

}
