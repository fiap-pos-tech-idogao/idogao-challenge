package br.com.idogz.adapter.driven.infra.order.persistence.entity;

import java.time.LocalDate;
import java.util.Set;

import br.com.idogz.adapter.driven.infra.client.persistence.entity.ClientEntity;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "tbOrder")
public class OrderEntity {

	@Id
	@Column(nullable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idOrder;

	@Column(nullable = false)
	private Integer ticketOrder;

	@Column(nullable = false)
	private LocalDate dateOrder;

	@Column(nullable = false)
	private Double totalValueOrder;

	@Column(length = 100)
	private String notesOrder;

	@ManyToOne()
	@JoinColumn(name = "idstatus", nullable = false)
	private StatusOrderEntity status;

	@ManyToOne
	@JoinColumn(name = "idclient", nullable = false)
	private ClientEntity client;

	@OneToMany(mappedBy = "id.order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<OrderProductEntity> orderProducts;

	public OrderEntity() {
		super();
	}

	public OrderEntity(final Integer idOrder, final Integer ticketOrder, final LocalDate dateOrder, final Double totalValueOrder,
			final String notesOrder, final StatusOrderEntity status, final ClientEntity client,
			final Set<OrderProductEntity> orderProducts) {
		this.idOrder = idOrder;
		this.ticketOrder = ticketOrder;
		this.dateOrder = dateOrder;
		this.totalValueOrder = totalValueOrder;
		this.notesOrder = notesOrder;
		this.status = status;
		this.client = client;
		this.orderProducts = orderProducts;
	}

	public Integer getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(final Integer idOrder) {
		this.idOrder = idOrder;
	}

	public Integer getTicketOrder() {
		return ticketOrder;
	}

	public void setTicketOrder(final Integer ticketOrder) {
		this.ticketOrder = ticketOrder;
	}

	public LocalDate getDateOrder() {
		return dateOrder;
	}

	public void setDateOrder(final LocalDate dateOrder) {
		this.dateOrder = dateOrder;
	}

	public Double getTotalValueOrder() {
		return totalValueOrder;
	}

	public void setTotalValueOrder(final Double totalValueOrder) {
		this.totalValueOrder = totalValueOrder;
	}

	public String getNotesOrder() {
		return notesOrder;
	}

	public void setNotesOrder(final String notesOrder) {
		this.notesOrder = notesOrder;
	}

	public StatusOrderEntity getStatus() {
		return status;
	}

	public void setStatus(final StatusOrderEntity status) {
		this.status = status;
	}

	public ClientEntity getClient() {
		return client;
	}

	public void setClient(final ClientEntity client) {
		this.client = client;
	}

	public Set<OrderProductEntity> getOrderProducts() {
		return orderProducts;
	}

	public void setOrderProducts(final Set<OrderProductEntity> orderProducts) {
		this.orderProducts = orderProducts;
	}

}
