package br.com.idogz.adapter.driven.infra.category.persistence.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "tbCategory", schema = "public")
public class CategoryEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idCategory")
	private Integer id;


    @Column(name = "nameCategory", nullable = false, unique = true, length = 50)
    private String name;

    public CategoryEntity() {
    }
    
    public CategoryEntity(Integer id, String name) {
        super();
		this.id = id;
        this.name = name;
    }

    public Integer id() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String name() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
