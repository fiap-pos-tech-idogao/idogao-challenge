package br.com.idogz.adapter.driven.infra.product.persistence.repository;

import java.util.Optional;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.idogz.adapter.driven.infra.product.persistence.entity.ProductEntity;
//import br.com.idogz.core.domain.Name;

public interface JpaPostgreSqlProductRepository extends JpaRepository<ProductEntity, Integer> {

	@Query(value = "select * from get_product_by_name(:_name)", nativeQuery = true)
	Optional<ProductEntity> getProductByName(@Param("_name") String name);
	
	@Query(value = "select * from get_product_by_id(:_productid)", nativeQuery = true)
	Optional<ProductEntity> getProductById(@Param("_productid") Integer productId);

	@Query(value = "select * from get_product_by_categoryid(:_categoryid)", nativeQuery = true)
	Optional<List<ProductEntity>> getProductByCategoryId(@Param("_categoryid") Integer categoryId);

}
