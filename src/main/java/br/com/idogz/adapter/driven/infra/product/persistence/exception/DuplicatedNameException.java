package br.com.idogz.adapter.driven.infra.product.persistence.exception;

import br.com.idogz.core.domain.Name;
import br.com.idogz.core.domain.exception.DomainException;

public class DuplicatedNameException extends DomainException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9189556126034136467L;
	
	public DuplicatedNameException(Name name) {
		super("Name already exists: " + name.value());
	}

}
