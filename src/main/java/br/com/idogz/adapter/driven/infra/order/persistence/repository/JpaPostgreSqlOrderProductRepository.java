package br.com.idogz.adapter.driven.infra.order.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.idogz.adapter.driven.infra.order.persistence.entity.OrderProductEntity;
import br.com.idogz.adapter.driven.infra.order.persistence.entity.IdOrderProduct;

public interface JpaPostgreSqlOrderProductRepository extends JpaRepository<OrderProductEntity, IdOrderProduct> {

	@Query(value = "SELECT * FROM public.create_order_product(:_idorder, :_idproduct, :_quantity, :_notes);", nativeQuery = true)
	void saveOrderProduct(@Param("_idorder") int idOrder, @Param("_idproduct") int idProduct,
			@Param("_quantity") int quantity, @Param("_notes") String notes);

}
