package br.com.idogz.adapter.driven.infra.category.persistence;

import br.com.idogz.adapter.driven.infra.category.persistence.entity.CategoryEntity;
import br.com.idogz.adapter.driven.infra.category.persistence.exception.CategoryIdNotFoundException;
import br.com.idogz.adapter.driven.infra.category.persistence.repository.CategoryJpaPostgreSqlRepository;
import br.com.idogz.core.application.factory.category.CategoryFactory;
import br.com.idogz.core.application.ports.category.CategoryRepository;
import br.com.idogz.core.domain.Category;
import br.com.idogz.core.domain.exception.DomainException;

public class CategoryRepositoryImpl implements CategoryRepository {

	private final CategoryJpaPostgreSqlRepository repository;
	private final CategoryFactory factory;

	public CategoryRepositoryImpl(final CategoryJpaPostgreSqlRepository repository, final CategoryFactory factory) {
		this.repository = repository;
		this.factory = factory;
	}

	@Override
	public Category findById(final Integer categoryId) throws DomainException {
		final var productEntity = repository.getCategoryById(categoryId)
				.orElseThrow(() -> new CategoryIdNotFoundException(categoryId));
		return toCategory(productEntity);
	}

	private Category toCategory(final CategoryEntity entity) throws DomainException {
		return factory.create(entity.id(), entity.name());
	}

}
