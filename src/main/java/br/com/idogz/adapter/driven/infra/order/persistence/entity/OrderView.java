package br.com.idogz.adapter.driven.infra.order.persistence.entity;

public interface OrderView {

	public Integer getIdOrder();

	public Integer getTicketOrder();

}
