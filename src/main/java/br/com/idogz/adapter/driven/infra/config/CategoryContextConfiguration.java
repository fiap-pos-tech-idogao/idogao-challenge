package br.com.idogz.adapter.driven.infra.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.idogz.adapter.driven.infra.category.persistence.CategoryRepositoryImpl;
import br.com.idogz.adapter.driven.infra.category.persistence.repository.CategoryJpaPostgreSqlRepository;
import br.com.idogz.core.application.factory.category.CategoryFactory;
import br.com.idogz.core.application.factory.category.CategoryFactoryImpl;
import br.com.idogz.core.application.ports.category.CategoryRepository;
import br.com.idogz.core.application.ports.category.CategoryService;
import br.com.idogz.core.application.services.category.CategoryServiceImpl;

@Configuration
public class CategoryContextConfiguration {

	@Bean
	CategoryService categoryService(CategoryRepository repository) {
		return new CategoryServiceImpl(repository);
	}
	
	@Bean
	CategoryRepository categoryRepository(CategoryJpaPostgreSqlRepository repository, CategoryFactory factory) {
		return new CategoryRepositoryImpl(repository, factory);
	}
	
	@Bean
	CategoryFactory categoryFactory() {
		return new CategoryFactoryImpl();
	}
	
}
