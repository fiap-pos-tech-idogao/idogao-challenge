package br.com.idogz.adapter.driven.infra.category.persistence.exception;

import java.text.MessageFormat;

import br.com.idogz.core.domain.exception.DomainException;

public class CategoryIdNotFoundException extends DomainException {

	private static final String CATEGORY_WITH_ID_NOT_FOUND = "Category with Id {0} not found";
	private static final long serialVersionUID = 6232136759238965693L;

	public CategoryIdNotFoundException(final Integer categoryId) {
		super(MessageFormat.format(CategoryIdNotFoundException.CATEGORY_WITH_ID_NOT_FOUND, categoryId));
	}
}
