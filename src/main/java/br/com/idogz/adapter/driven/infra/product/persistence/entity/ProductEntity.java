package br.com.idogz.adapter.driven.infra.product.persistence.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import br.com.idogz.adapter.driven.infra.category.persistence.entity.CategoryEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tbProduct", schema = "public")
public class ProductEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idProduct")
	private Integer id;

    @Column(nullable = false, length = 100)
    private String name;

    @Column(nullable = false)
    private Double priceProduct;

    @Column(nullable = false, length = 200)
    private String description;

    @Column(nullable = false)
    private Boolean active;

    @Column(nullable = true)
    private String urlImage;

    @Fetch(FetchMode.JOIN)
	@ManyToOne()
	@JoinColumn(name = "idCategory", nullable = false)
	private CategoryEntity category;

    public ProductEntity() {
	}

	public ProductEntity(final Integer id, final CategoryEntity category, final String name, final Double price, final String description, final Boolean active, final String urlImage) {
		this.id = id;
        this.name = name;
        this.category = category;
		this.priceProduct = price;
		this.description = description;
		this.active = active;
        this.urlImage = urlImage;
	}

    public Integer id() {
        return id;
    }

    public void setIdProduct(final Integer id) {
        this.id = id;
    }
    
	public CategoryEntity category() {
		return category;
	}

	public void setCategory(final CategoryEntity category) {
		this.category = category;
	}

    public String name() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Double price() {
        return priceProduct;
    }

    public void setPrice(final Double priceProduct) {
        this.priceProduct = priceProduct;
    }

    public String description() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Boolean active() {
        return active;
    }

    public void setActive(final Boolean active) {
        this.active = active;
    }

    public String urlImage() {
        return urlImage;
    }

    public void setUrlImage(final String urlImage) {
        this.urlImage = urlImage;
    }
}
