package br.com.idogz.adapter.driven.infra.order.persistence.specification;

import org.springframework.data.jpa.domain.Specification;

import br.com.idogz.adapter.driven.infra.order.persistence.entity.OrderEntity;
import jakarta.persistence.criteria.JoinType;

public class OrderSpecifications {

	public static Specification<OrderEntity> all() {
		return (root, query, criteriaBuilder) -> {
			root.fetch("client", JoinType.INNER);
			root.fetch("status", JoinType.INNER);
			root.fetch("orderProducts", JoinType.INNER);
			return criteriaBuilder.conjunction();
		};
	}

}
