package br.com.idogz.adapter.driven.infra.product.persistence.exception;

import br.com.idogz.core.domain.exception.DomainException;

public class ProductIdNotFoundException extends DomainException {

	private static final long serialVersionUID = 6232136759238965693L;

	public ProductIdNotFoundException(Integer idProduct) {
		super("Product with Id " + idProduct + " not found");
	}
}
