package br.com.idogz.adapter.driven.infra.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.idogz.adapter.driven.infra.client.persistence.ClientRepositoryImpl;
import br.com.idogz.adapter.driven.infra.client.persistence.repository.ClientJpaPostgreSqlRepository;
import br.com.idogz.core.application.factory.client.ClientFactory;
import br.com.idogz.core.application.factory.client.ClientFactoryImpl;
import br.com.idogz.core.application.ports.client.ClientRepository;
import br.com.idogz.core.application.ports.client.ClientService;
import br.com.idogz.core.application.services.client.ClientServiceImpl;

@Configuration
public class ClientContextConfiguration {

	@Bean
	ClientService clientService(ClientRepository repository) {
		return new ClientServiceImpl(repository);
	}
	
	@Bean
	ClientRepository clientRepository(ClientJpaPostgreSqlRepository repository, ClientFactory factory) {
		return new ClientRepositoryImpl(repository, factory);
	}
	
	@Bean
	ClientFactory clientFactory() {
		return new ClientFactoryImpl();
	}
	
}
