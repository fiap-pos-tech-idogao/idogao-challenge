package br.com.idogz.adapter.driven.infra.product.persistence.exception;

import br.com.idogz.core.domain.Name;
import br.com.idogz.core.domain.exception.DomainException;

public class ProductNameNotFoundException extends DomainException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6232136759238965693L;

	public ProductNameNotFoundException(Name name) {
		super("Product with name " + name.value() + " not found");
	}
}
