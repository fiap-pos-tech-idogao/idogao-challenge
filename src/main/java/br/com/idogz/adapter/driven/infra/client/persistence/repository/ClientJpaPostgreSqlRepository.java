package br.com.idogz.adapter.driven.infra.client.persistence.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.idogz.adapter.driven.infra.client.persistence.entity.ClientEntity;

public interface ClientJpaPostgreSqlRepository extends JpaRepository<ClientEntity, Integer> {

	@Query(value = "select * from get_client_by_cpf(:_cpf)", nativeQuery = true)
	Optional<ClientEntity> getClientByCpf(@Param("_cpf") String cpf);
	
}
