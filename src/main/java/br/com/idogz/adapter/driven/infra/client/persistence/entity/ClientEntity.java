package br.com.idogz.adapter.driven.infra.client.persistence.entity;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedStoredProcedureQuery;
import jakarta.persistence.ParameterMode;
import jakarta.persistence.StoredProcedureParameter;
import jakarta.persistence.Table;

@Entity
@Table(name = "tbClient", schema = "public")
@NamedStoredProcedureQuery(name = "getClientByCpf", procedureName = "get_client_by_cpf", resultClasses = ClientEntity.class, parameters = {
		@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class) })
public class ClientEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idClient")
	private Integer id;

	@Column(name = "name", length = 200)
	private String name;

	@Column(name = "cpf", length = 11)
	private String cpf;

	@Column(name = "email")
	private String email;

	public ClientEntity() {
	}

	public ClientEntity(final Integer id, final String name, final String cpf, final String email) {
		super();
		this.id = id;
		this.name = name;
		this.cpf = cpf;
		this.email = email;
	}

	public Integer id() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String name() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String cpf() {
		return cpf;
	}

	public void setCpf(final String cpf) {
		this.cpf = cpf;
	}

	public String email() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@Override
	public int hashCode() {
		return Objects.hash(cpf, email, id, name);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if ((obj == null) || (getClass() != obj.getClass())) {
			return false;
		}
		final var other = (ClientEntity) obj;
		return Objects.equals(cpf, other.cpf) && Objects.equals(email, other.email) && Objects.equals(id, other.id)
				&& Objects.equals(name, other.name);
	}

}
