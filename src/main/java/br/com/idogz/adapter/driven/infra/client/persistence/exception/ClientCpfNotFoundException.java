package br.com.idogz.adapter.driven.infra.client.persistence.exception;

import java.text.MessageFormat;

import br.com.idogz.core.domain.Cpf;
import br.com.idogz.core.domain.exception.DomainException;

public class ClientCpfNotFoundException extends DomainException {

	private static final String CLIENT_WITH_CPF_VALUE_NOT_FOUND = "Client with cpf {0} not found";
	/**
	 *
	 */
	private static final long serialVersionUID = 6232136759238965693L;

	public ClientCpfNotFoundException(final Cpf cpf) {
		super(MessageFormat.format(ClientCpfNotFoundException.CLIENT_WITH_CPF_VALUE_NOT_FOUND, cpf.value()));
	}

}
