package br.com.idogz.adapter.driven.infra.order.persistence.entity;

import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "tbStatusOrder", schema = "public")
public class StatusOrderEntity {

	@Id
	@Column(nullable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer idStatus;

	@Column(nullable = false, unique = true, length = 100)
	private String description;

	@OneToMany(mappedBy = "status")
	private Set<OrderEntity> statusOrders;

	public StatusOrderEntity() {
	}

	public StatusOrderEntity(final Integer idStatus, final String description) {
		this.idStatus = idStatus;
		this.description = description;
	}

	public Integer getId() {
		return idStatus;
	}

	public void setIdStatus(final Integer idStatus) {
		this.idStatus = idStatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public Set<OrderEntity> getStatusOrders() {
		return statusOrders;
	}

	public void setStatusOrders(final Set<OrderEntity> statusOrders) {
		this.statusOrders = statusOrders;
	}

}
