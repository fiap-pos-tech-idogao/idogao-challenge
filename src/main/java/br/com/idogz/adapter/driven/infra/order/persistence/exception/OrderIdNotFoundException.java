package br.com.idogz.adapter.driven.infra.order.persistence.exception;

import br.com.idogz.core.domain.exception.DomainException;

public class OrderIdNotFoundException extends DomainException {

	/**
	 *
	 */
	private static final long serialVersionUID = 2579252225932638145L;

	public OrderIdNotFoundException(final int id) {
		super("Order with id " + id + " not found");
	}

}
