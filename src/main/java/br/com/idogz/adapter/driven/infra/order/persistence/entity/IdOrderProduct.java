package br.com.idogz.adapter.driven.infra.order.persistence.entity;

import java.io.Serializable;
import java.util.Objects;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import br.com.idogz.adapter.driven.infra.product.persistence.entity.ProductEntity;
import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Embeddable
public class IdOrderProduct implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8590363244634826472L;

	@Fetch(FetchMode.JOIN)
	@ManyToOne()
	@JoinColumn(name = "idOrder")
	private OrderEntity order;

	@Fetch(FetchMode.JOIN)
	@ManyToOne()
	@JoinColumn(name = "idProduct", nullable = false)
	private ProductEntity product;

	public IdOrderProduct() {
	}

	public IdOrderProduct(final OrderEntity order, final ProductEntity product) {
		this.order = order;
		this.product = product;
	}

	public OrderEntity getOrder() {
		return order;
	}

	public void setOrder(final OrderEntity order) {
		this.order = order;
	}

	public ProductEntity getProduct() {
		return product;
	}

	public void setProduct(final ProductEntity product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "OrderProductId [order=" + order + ", product=" + product + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(order, product);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if ((obj == null) || (getClass() != obj.getClass())) {
			return false;
		}
		final var other = (IdOrderProduct) obj;
		return Objects.equals(order, other.order) && Objects.equals(product, other.product);
	}

}
