package br.com.idogz.adapter.driven.infra.client.persistence.exception;

import java.text.MessageFormat;

import br.com.idogz.core.domain.exception.DomainException;

public class ClientIdNotFoundException extends DomainException {

	private static final String CLIENT_WITH_ID_NOT_FOUND = "Client with id {0} not found";
	/**
	 *
	 */
	private static final long serialVersionUID = 1209491140167050769L;

	public ClientIdNotFoundException(final int id) {
		super(MessageFormat.format(ClientIdNotFoundException.CLIENT_WITH_ID_NOT_FOUND, id));
	}

}
