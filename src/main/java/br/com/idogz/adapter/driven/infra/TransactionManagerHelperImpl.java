package br.com.idogz.adapter.driven.infra;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import br.com.idogz.core.application.ports.transaction.TransactionManagerHelper;

public class TransactionManagerHelperImpl implements TransactionManagerHelper {

	private final PlatformTransactionManager transactionManager;
	private TransactionStatus status;

	public TransactionManagerHelperImpl(final PlatformTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	@Override
	public void startTransaction(final String transactionName) throws TransactionException {
		final var def = new DefaultTransactionDefinition();
		def.setName(transactionName);
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		status = transactionManager.getTransaction(def);
	}

	@Override
	public void commit() throws TransactionException {
		transactionManager.commit(status);
	}

	@Override
	public void rollback() throws TransactionException {
		transactionManager.rollback(status);
	}


}
