package br.com.idogz.adapter.driver.web.category.model;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.idogz.adapter.driver.web.client.ClientController;
import br.com.idogz.core.domain.Category;

@Component
public class CategoryRepresentationModelAssembler extends RepresentationModelAssemblerSupport<Category, CategoryModel> {
	
	public CategoryRepresentationModelAssembler() {
		super(ClientController.class, CategoryModel.class);
	}

	@Override
	public CategoryModel toModel(Category category) {
		final var model = new CategoryModel(category.id(), category.name());

		model.add(Link.of("/category/{name}").withRel(LinkRelation.of("category")).expand(model.name));

		return model;
	}
}
