package br.com.idogz.adapter.driver.web.client;

import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.idogz.adapter.driver.web.ApiError;
import br.com.idogz.adapter.driver.web.client.model.ClientModel;
import br.com.idogz.core.application.factory.client.ClientFactory;
import br.com.idogz.core.application.ports.client.ClientService;
import br.com.idogz.core.domain.Client;
import br.com.idogz.core.domain.Cpf;
import br.com.idogz.core.domain.exception.DomainException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Client", description = "Client Management API")
@RestController
@RequestMapping(path = "/client")
@ExposesResourceFor(ClientModel.class)
public class ClientController {

	private final ClientService service;
	private final RepresentationModelAssemblerSupport<Client, ClientModel> modelAssembler;
	private final ClientFactory factory;

	public ClientController(final ClientService clientService,
			final RepresentationModelAssemblerSupport<Client, ClientModel> modelAssembler, final ClientFactory factory) {
		service = clientService;
		this.modelAssembler = modelAssembler;
		this.factory = factory;
	}

	@GetMapping("/{cpf}")
	@Operation(summary = "Search for a Client by its CPF",
	description = "Get the Client's information by specifying the CPF. The response is a object with: Name, CPF, Email and Client internal ID.",
	tags = { "get" })
	@Parameter(name = "cpf",
	description = "Client's by CPF",
	schema = @Schema(
			description = "Client's by CPF",
			example = "712.955.208-50",
			minLength = 11,
			maxLength = 14,
			format = "\\d{3}.\\d{3}.\\d{3}-\\d{2}"
			)
			)
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200",
					description = "Client's found",
					content = {
							@Content(
									mediaType = "application/json",
									schema = @Schema(implementation = ClientModel.class)
									)
			}
					),
			@ApiResponse(responseCode = "400", description = "Invalid CPF provided", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
			@ApiResponse(responseCode = "404", description = "Client's not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))
	})
	public ResponseEntity<ClientModel> find(@PathVariable final String cpf) throws DomainException {
		final var client = service.find(new Cpf(cpf));
		final var model = modelAssembler.toModel(client);
		return ResponseEntity.ok(model);
	}

	@PostMapping()
	@Operation(summary = "Saves a new Client data",
	description = "Create a new Customer record with the data provided in the request.",
	tags = { "post" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Client created", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ClientModel.class))),
			@ApiResponse(responseCode = "400", description = "Client creation failed", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))
	})
	public ResponseEntity<ClientModel> save(@RequestBody final CreateClientRequestDto request) throws DomainException {
		final var client = factory.create(request.name(), request.cpf(), request.email());
		final var savedClient = service.create(client);
		final var model = modelAssembler.toModel(savedClient);
		return ResponseEntity.ok(model);
	}

}
