package br.com.idogz.adapter.driver.web.order.model;

import java.util.Objects;

import org.springframework.hateoas.RepresentationModel;

import br.com.idogz.adapter.driver.web.product.model.ProductModel;

public class OrderProductModel extends RepresentationModel<OrderProductModel> {

	public ProductModel product;
	public int quantity;
	public String notes;

	public OrderProductModel(final ProductModel product, final int quantity, final String notes) {
		this.product = product;
		this.quantity = quantity;
		this.notes = notes;
	}

	@Override
	public int hashCode() {
		final var prime = 31;
		var result = super.hashCode();
		result = prime * result + Objects.hash(notes, product, quantity);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj) || (getClass() != obj.getClass())) {
			return false;
		}
		final var other = (OrderProductModel) obj;
		return Objects.equals(notes, other.notes) && Objects.equals(product, other.product)
				&& quantity == other.quantity;
	}

}
