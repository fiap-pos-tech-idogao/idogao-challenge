package br.com.idogz.adapter.driver.web.order;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import br.com.idogz.adapter.driver.web.ApiError;
import br.com.idogz.adapter.driver.web.order.model.OrderModel;
import br.com.idogz.core.application.factory.order.OrderBuilder;
import br.com.idogz.core.application.ports.order.OrderService;
import br.com.idogz.core.domain.Order;
import br.com.idogz.core.domain.exception.DomainException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Order", description = "Order Management API")
@RestController
@RequestMapping(path = "/order")
@ExposesResourceFor(OrderModel.class)
public class OrderController {

	private final OrderService orderService;
	private final OrderBuilder builder;
	private final RepresentationModelAssemblerSupport<Order, OrderModel> modelAssemblerSupport;

	public OrderController(final OrderService orderservice, final OrderBuilder builder,
			final RepresentationModelAssemblerSupport<Order, OrderModel> modelAssemblerSupport) {
		orderService = orderservice;
		this.builder = builder;
		this.modelAssemblerSupport = modelAssemblerSupport;
	}

	@GetMapping("/{id}")
	@Operation(summary = "Search for a specific order by its idOrder.",
	description = "Get the Order information by specifying the Order internal ID. The response is an object with data about the Order.",
	tags = { "get" })
	@Parameter(name = "id",
	description = "Order's by Order internal ID",
	schema = @Schema(
		description = "Order's by Order internal ID",
		example = "1"
		))
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Order found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = OrderModel.class))),
			@ApiResponse(responseCode = "404", description = "Order not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))) })
	public ResponseEntity<OrderModel> find(@PathVariable("id") final int id) throws DomainException {
		final var order = orderService.findById(id);
		final var model = modelAssemblerSupport.toModel(order);
		return ResponseEntity.ok(model);
	}

	@GetMapping
	@Operation(summary = "Search all order information.",
	description = "Get information for all orders. The response is a list of objects with data about the Order.",
	tags = { "get" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Orders founds", content = @Content(mediaType = "application/json", schema = @Schema(implementation = OrderModel.class))) })
	public ResponseEntity<CollectionModel<OrderModel>> list() throws DomainException {
		final var orders = orderService.allOrders();
		final var collectionModel = modelAssemblerSupport.toCollectionModel(orders);
		return ResponseEntity.ok(collectionModel);
	}

	@PostMapping
	@Operation(summary = "Create a new Order",
	description = "Save information related to a Client's new Order",
	tags = { "post" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Order created", content = @Content(mediaType = "application/json", schema = @Schema(implementation = OrderModel.class))),
			@ApiResponse(responseCode = "400", description = "Order creation failed", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))) })
	public ResponseEntity<OrderModel> create(@RequestBody final CreateOrderRequestDto entity) throws DomainException {
		final var order = toOrder(entity);
		final var savedOrder = orderService.create(order);
		final var model = modelAssemblerSupport.toModel(savedOrder);
		final var uri = MvcUriComponentsBuilder
				.fromMethodCall(MvcUriComponentsBuilder.on(OrderController.class).find(order.id())).buildAndExpand(1)
				.toUri();
		return ResponseEntity.created(uri).body(model);
	}

	private Order toOrder(final CreateOrderRequestDto request) throws DomainException {
		builder.client(request.clientId());
		builder.date(request.date());
		builder.notes(request.notes());
		addProducts(request);
		return builder.build();
	}

	private void addProducts(final CreateOrderRequestDto entity) throws DomainException {
		for (final OrderProductRequest orderProductRequest : entity.orderProducts()) {
			builder.addProduct(orderProductRequest.idProduct(), orderProductRequest.quantity(), 0, orderProductRequest.notes());
		}
	}

}
