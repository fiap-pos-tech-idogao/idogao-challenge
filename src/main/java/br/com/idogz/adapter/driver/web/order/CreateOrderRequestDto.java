package br.com.idogz.adapter.driver.web.order;

import java.time.LocalDate;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

@Schema(description = "Order Model Information")
public record CreateOrderRequestDto(
		@Schema(accessMode = AccessMode.READ_ONLY, description = "Ticket Order, resets every day", example = "1")
		int ticketOrder,
		@Schema(description = "Date when Order where created", example = "15/05/2024")
		LocalDate date,
		@Schema(description = "Internal ID of the Client who created the Order", example = "1")
		int clientId,
		@Schema(description = "General Observations about the Order", example = "Toque a campainha, por favor.")
		String notes,
		@Schema(description = "List of Products ordered in the Order")
		List<OrderProductRequest> orderProducts
		) {

}
