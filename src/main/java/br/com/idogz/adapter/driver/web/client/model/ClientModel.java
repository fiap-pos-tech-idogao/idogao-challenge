package br.com.idogz.adapter.driver.web.client.model;

import java.util.Objects;

import org.springframework.hateoas.RepresentationModel;

public class ClientModel extends RepresentationModel<ClientModel> {

	public Integer id;
	public String name;
	public String cpf;
	public String email;

	public ClientModel(final Integer id, final String name, final String cpf, final String email) {
		this.id = id;
		this.name = name;
		this.cpf = cpf;
		this.email = email;
	}

	@Override
	public int hashCode() {
		final var PRIME = 31;
		var result = super.hashCode();
		result = PRIME * result + Objects.hash(cpf, email, id, name);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj) || (getClass() != obj.getClass())) {
			return false;
		}
		final var other = (ClientModel) obj;
		return Objects.equals(cpf, other.cpf) && Objects.equals(email, other.email) && Objects.equals(id, other.id)
				&& Objects.equals(name, other.name);
	}



}
