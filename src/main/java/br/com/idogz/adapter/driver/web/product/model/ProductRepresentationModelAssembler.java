package br.com.idogz.adapter.driver.web.product.model;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.idogz.adapter.driver.web.client.ClientController;
import br.com.idogz.core.domain.Product;

@Component
public class ProductRepresentationModelAssembler extends RepresentationModelAssemblerSupport<Product, ProductModel> {

	public ProductRepresentationModelAssembler() {
		super(ClientController.class, ProductModel.class);
	}

	@Override
	public ProductModel toModel(final Product product) {
		final var model = new ProductModel(product.id(), product.category().id(), product.name(), product.price(),
				product.description(), product.active(), product.urlImage());

		model.add(Link.of("/product/{name}").withRel(LinkRelation.of("product")).expand(model.name));

		return model;
	}
}
