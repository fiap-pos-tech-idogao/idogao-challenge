package br.com.idogz.adapter.driver.web.client.model;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.idogz.adapter.driver.web.client.ClientController;
import br.com.idogz.core.domain.Client;

@Component
public class ClientRepresentationModelAssembler extends RepresentationModelAssemblerSupport<Client, ClientModel> {

	public ClientRepresentationModelAssembler() {
		super(ClientController.class, ClientModel.class);
	}

	@Override
	public ClientModel toModel(final Client client) {
		final var model = new ClientModel(client.id(), client.name(), client.formattingCpf(),
				client.emailAddress());

		model.add(Link.of("/client/{cpf}").withRel(LinkRelation.of("client")).expand(model.cpf));

		return model;
	}

}
