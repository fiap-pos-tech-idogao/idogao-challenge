package br.com.idogz.adapter.driver.web.order.model;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.idogz.adapter.driver.web.client.model.ClientModel;
import br.com.idogz.adapter.driver.web.order.OrderController;
import br.com.idogz.adapter.driver.web.product.model.ProductModel;
import br.com.idogz.core.domain.Order;
import br.com.idogz.core.domain.OrderProduct;
import br.com.idogz.core.domain.Product;

@Component
public class OrderRepresentationModelAssembler extends RepresentationModelAssemblerSupport<Order, OrderModel> {

	private final EntityLinks entityLinks;

	public OrderRepresentationModelAssembler(final EntityLinks entityLinks) {
		super(OrderController.class, OrderModel.class);
		this.entityLinks = entityLinks;
	}

	@Override
	public OrderModel toModel(final Order order) {
		final var orderProducts = order.products().stream().map(OrderRepresentationModelAssembler::toOrderProductModel)
				.toList();
		final var model = new OrderModel(order.id(), order.date(), order.ticket().value(),
				order.status().getDescription(), order.client().id(), order.notes().getValue(), orderProducts);
		model.add(Link.of("/order/{id}").withRel(LinkRelation.of("order")).expand(model.id));
		model.add(entityLinks.linkToItemResource(ClientModel.class, order.client().id()));
		return model;
	}

	private static OrderProductModel toOrderProductModel(final OrderProduct orderProduct) {
		final var productModel = OrderRepresentationModelAssembler.toProductModel(orderProduct.product());
		return new OrderProductModel(productModel, orderProduct.quantity(), orderProduct.notes());
	}

	private static ProductModel toProductModel(final Product product) {
		return new ProductModel(product.id(), product.category().id(), product.name(), product.price(),
				product.description(), product.active(), product.urlImage());
	}

}
