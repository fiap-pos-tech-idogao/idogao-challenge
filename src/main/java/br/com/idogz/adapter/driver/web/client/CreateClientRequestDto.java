package br.com.idogz.adapter.driver.web.client;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

@Schema(description = "Client Model Information")
public record CreateClientRequestDto(
		@Schema(accessMode = AccessMode.READ_ONLY, description = "Client internal ID", example = "1")
		Integer id,
		
		@Schema(description = "Client's Name", example = "Steve Trabalhos Varying")
		String name,
		
		@Schema(description = "Client's CPF", example = "712.955.208-50", minLength = 11, maxLength = 14, format = "\\d{3}.\\d{3}.\\d{3}-\\d{2}")
		String cpf,
		
		@Schema(description = "Client's Email", example = "steve@idoguetz.com")
		String email) {

}
