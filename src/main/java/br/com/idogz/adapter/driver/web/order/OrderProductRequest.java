package br.com.idogz.adapter.driver.web.order;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Order Model Information")
public record OrderProductRequest(
		@Schema(description = "Product internal ID", example = "1")
		int idProduct,
		@Schema(description = "Product quantity", example = "3")
		int quantity,
		@Schema(description = "Product observations and notes", example = "Mostarda a parte")
		String notes) {

}
