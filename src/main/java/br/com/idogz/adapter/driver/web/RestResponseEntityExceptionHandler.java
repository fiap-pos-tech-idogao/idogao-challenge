package br.com.idogz.adapter.driver.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.idogz.core.domain.exception.DomainException;
import br.com.idogz.core.domain.exception.ValidationException;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = ValidationException.class)
	protected ResponseEntity<ApiError> handleInvalidException(final Exception ex, final WebRequest request) {
		ex.printStackTrace();
		final var apiError = new ApiError(HttpStatus.BAD_REQUEST, "Validation Error", ex);
		return ResponseEntity.status(apiError.getStatus()).body(apiError);
	}

	@ExceptionHandler(value = DomainException.class)
	protected ResponseEntity<ApiError> handleDomainException(final Exception ex, final WebRequest request) {
		ex.printStackTrace();
		final var apiError = new ApiError(HttpStatus.BAD_REQUEST, "Business Rules Error", ex);
		return ResponseEntity.status(apiError.getStatus()).body(apiError);
	}

}
