package br.com.idogz.adapter.driver.web.product;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import br.com.idogz.adapter.driver.web.ApiError;
import br.com.idogz.adapter.driver.web.client.model.ClientModel;
import br.com.idogz.adapter.driver.web.product.model.ProductModel;
import br.com.idogz.core.application.factory.product.ProductFactory;
import br.com.idogz.core.application.ports.product.ProductService;
import br.com.idogz.core.domain.Name;
import br.com.idogz.core.domain.Product;
import br.com.idogz.core.domain.exception.DomainException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Product", description = "Product Management API")
@RestController
@RequestMapping(path = "/product")
public class ProductController {

	private final ProductService productService;
	private final RepresentationModelAssemblerSupport<Product, ProductModel> modelAssembler;
	private final ProductFactory factory;

	public ProductController(final ProductService productService,
			final RepresentationModelAssemblerSupport<Product, ProductModel> modelAssembler,
			final ProductFactory factory) {
		this.productService = productService;
		this.modelAssembler = modelAssembler;
		this.factory = factory;
	}

	@GetMapping("/idProduct/{idProduct}")
	@Operation(summary = "Search for a Product by its idProduct",
	description = "Get the Product information by specifying the Product internal ID. The response is a object with: Name, Description, Price, Active status, Image URL, the Category internal ID and the Product internal ID.",
	tags = { "get" })
	@Parameter(name = "idProduct",
	description = "Product's by Product internal ID",
	schema = @Schema(
			description = "Product's by Product internal ID",
			example = "1",
			minLength = 1,
			maxLength = 2147483647
			)
			)
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Product found", content = { @Content( mediaType = "application/json", schema = @Schema(implementation = ProductModel.class))}),
			@ApiResponse(responseCode = "400", description = "Invalid idProduct provided", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
			@ApiResponse(responseCode = "404", description = "Product not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))
	})
	public ResponseEntity<ProductModel> findById(@PathVariable final Integer idProduct) throws DomainException {
		final var product = productService.findById(idProduct);
		final var model = modelAssembler.toModel(product);
		return ResponseEntity.ok(model);
	}

	@GetMapping("/idCategory/{idCategory}")
	@Operation(summary = "Search for a Product by its idCategory",
	description = "Get the Product information by specifying the Category internal ID. The response is a object with: Name, Description, Price, Active status, Image URL, the Category internal ID and the Product internal ID.",
	tags = { "get" })
	@Parameter(name = "idCategory",
	description = "Product's by Category internal ID",
	schema = @Schema( 
		description = "Product's by Category internal ID",
		example = "1",
		type = "integer",
		maxLength = 2147483647))
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Product found", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = ProductModel.class)) }),
			@ApiResponse(responseCode = "400", description = "Invalid idCategory for Product provided", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
			@ApiResponse(responseCode = "404", description = "Product not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))) })
	public ResponseEntity<CollectionModel<ProductModel>> findByCategoryId(@PathVariable final Integer idCategory)
			throws DomainException {
		final var products = productService.findByCategoryId(idCategory);
		final var model = modelAssembler.toCollectionModel(products);
		return ResponseEntity.ok(model);
	}

	@GetMapping("/name/{name}")
	@Operation(summary = "Search for a Product by its Name",
	description = "Get the Product information by exact Name. The response is a object with: Name, Description, Price, Active status, Image URL, the Category internal ID and the Product internal ID.",
	tags = { "get" })
	@Parameter(name = "name",
	description = "Product's by Name",
	schema = @Schema( 
		description = "Product's by exact Name",
		example = "Cachorro-quente do iDogz",
		minLength = 1,
		maxLength = 100))
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Product found", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ProductModel.class)) }),
			@ApiResponse(responseCode = "400", description = "Invalid Name for Product provided", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
			@ApiResponse(responseCode = "404", description = "Product not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))) })
	public ResponseEntity<ProductModel> findByName(@PathVariable final String name) throws DomainException {
		final var product = productService.findByName(new Name(name));
		final var model = modelAssembler.toModel(product);
		return ResponseEntity.ok(model);
	}

	@PostMapping()
	@Operation(summary = "Create a new Product",
	description = "Create a new Product record with the data provided in the request.",
	tags = { "post" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Product created", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ClientModel.class))),
			@ApiResponse(responseCode = "400", description = "Product creation failed", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))) })
	public ResponseEntity<ProductModel> create(@RequestBody final CreateProductRequestDto request) throws DomainException {
		final var product = factory.create(request.idCategory(), request.name(), request.price(), request.description(),
				request.active(), request.urlImage());
		final var savedProduct = productService.create(product);
		final var model = modelAssembler.toModel(savedProduct);

		final var uri = MvcUriComponentsBuilder
			.fromMethodCall(MvcUriComponentsBuilder.on(ProductController.class).findById(product.id())).buildAndExpand(1)
			.toUri();

		return ResponseEntity.created(uri).body(model);
	}

	@PutMapping()
	@Operation(summary = "Update data for an existing Product",
	description = "Update an existing Product record with the data provided in the request.",
	tags = { "post" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Product update", content = @Content(mediaType = "application/json")),
			@ApiResponse(responseCode = "400", description = "Product updated failed", content = @Content(mediaType = "application/json")) })
	public void update(@RequestBody final UpdateProductRequestDto request) throws DomainException {
		final var product = factory.create(request.id(), request.idCategory(), request.name(), request.price(),
				request.description(), request.active(), request.urlImage());
		productService.update(product);
		ResponseEntity.ok(true);
	}

	@DeleteMapping("/idProduct/{idProduct}")
	@Operation(summary = "Remove data from an existing Product",
	description = "Deactivate an existing Product with the data provided in the request.",
	tags = { "post" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Product remove", content = @Content(mediaType = "application/json")),
			@ApiResponse(responseCode = "400", description = "Product remove failed", content = @Content(mediaType = "application/json")) })
	public void remove(@PathVariable final Integer idProduct) throws DomainException {
		productService.remove(idProduct);
		ResponseEntity.ok(true);
	}

}
