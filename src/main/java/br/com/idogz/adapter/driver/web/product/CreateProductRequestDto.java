package br.com.idogz.adapter.driver.web.product;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

@Schema(description = "Product Model Information")
public record CreateProductRequestDto(
		@Schema(accessMode = AccessMode.READ_ONLY, description = "Product internal ID", example = "2")
		Integer id,

		@Schema(description = "Category internal ID", example = "1")
		Integer idCategory,

		@Schema(description = "Product's Name", example = "Cachorro-quente do iDogz")
		String name,

		@Schema(description = "Product's Price", example = "22.22")
		Double price,

		@Schema(description = "Products's Description", example = "Pão de hot-dog, Salsicha tradicional, Pure de batatas, Batata palha, Ketchup e Mostarda",
		minLength = 11, maxLength = 14, format = "\\d{3}.\\d{3}.\\d{3}-\\d{2}")
		String description,

		@Schema(description = "Product's Active", example = "True")
		Boolean active,

		@Schema(description = "Product's URL Image", example = "s3://idogz/products/snacks/cachorro_quente_idogz.png")
		String urlImage) {

}
