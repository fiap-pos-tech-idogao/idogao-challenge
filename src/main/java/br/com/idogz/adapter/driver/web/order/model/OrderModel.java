package br.com.idogz.adapter.driver.web.order.model;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.RepresentationModel;

import br.com.idogz.adapter.driver.web.client.model.ClientModel;

public class OrderModel extends RepresentationModel<ClientModel> {

	public int id;
	public LocalDate date;
	public int ticketOrder;
	public String status;
	public int clientId;
	public String notes;
	public List<OrderProductModel> orderProducts;

	public OrderModel(final int id, final LocalDate date, final int ticketOrder, final String status,
			final int clientId, final String notes, final List<OrderProductModel> orderProducts) {
		this.id = id;
		this.date = date;
		this.ticketOrder = ticketOrder;
		this.status = status;
		this.clientId = clientId;
		this.notes = notes;
		this.orderProducts = orderProducts;
	}

	@Override
	public int hashCode() {
		final var PRIME = 31;
		var result = super.hashCode();
		result = PRIME * result + Objects.hash(clientId, date, id, notes, orderProducts, status, ticketOrder);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj) || (getClass() != obj.getClass())) {
			return false;
		}
		final var other = (OrderModel) obj;
		return clientId == other.clientId && Objects.equals(date, other.date) && id == other.id
				&& Objects.equals(notes, other.notes) && Objects.equals(orderProducts, other.orderProducts)
				&& Objects.equals(status, other.status) && ticketOrder == other.ticketOrder;
	}

}
