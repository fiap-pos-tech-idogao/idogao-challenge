package br.com.idogz.adapter.driver.web.category;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.idogz.adapter.driver.web.ApiError;
import br.com.idogz.adapter.driver.web.category.model.CategoryModel;
import br.com.idogz.core.application.ports.category.CategoryService;
import br.com.idogz.core.domain.Category;
import br.com.idogz.core.domain.exception.DomainException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Category", description = "Category Management API")
@RestController
@RequestMapping(path = "/category")
public class CategoryController {

	private final CategoryService categoryService;
	private final RepresentationModelAssemblerSupport<Category, CategoryModel> modelAssembler;

	public CategoryController(final CategoryService categoryService,
			final RepresentationModelAssemblerSupport<Category, CategoryModel> modelAssembler) {
		this.categoryService = categoryService;
		this.modelAssembler = modelAssembler;
	}

	@GetMapping("/idCategory/{idCategory}")
	@Operation(summary = "Search for a Category by its idCategory",
	description = "Get the Category information by specifying the Category internal ID. The response is an object with: Name, Category internal ID and Product internal ID.",
	tags = { "get" })
	@Parameter(name = "idCategory",
	description = "Category by Category internal ID",
	schema = @Schema(
			description = "Category by Category internal ID",
			example = "1",
			maxLength = 2_147_483_647,
			type = "integer"
			)
			)
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200",
					description = "Category found",
					content = {
							@Content(
									mediaType = "application/json",
									schema = @Schema(implementation = CategoryModel.class)
									)
			}
					),
			@ApiResponse(responseCode = "400", description = "Invalid idCategory provided", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
			@ApiResponse(responseCode = "404", description = "Category not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))
	})
	public ResponseEntity<CategoryModel> findById(@PathVariable final Integer idCategory) throws DomainException {
		final var category = categoryService.findById(idCategory);
		final var model = modelAssembler.toModel(category);
		return ResponseEntity.ok(model);
	}

}
