package br.com.idogz.core.application.factory.category;

import br.com.idogz.core.domain.Category;
import br.com.idogz.core.domain.Name;
import br.com.idogz.core.domain.exception.DomainException;

public class CategoryFactoryImpl implements CategoryFactory {

	@Override
	public Category create(final String name) throws DomainException {
		final var nameVO = new Name(name);
		return new Category(nameVO);
	}

	@Override
	public Category create(final Integer id, final String name) throws DomainException {
		final var nameVO = new Name(name);
		final var category = new Category(nameVO);
		category.updateId(id);
		return category;
	}
}
