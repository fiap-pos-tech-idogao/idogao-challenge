package br.com.idogz.core.application.factory.product;

import br.com.idogz.core.domain.Product;
import br.com.idogz.core.domain.exception.DomainException;

public interface ProductFactory {

	Product create(Integer categoryId, String name, Double price, String description, Boolean active, String urlImage)
			throws DomainException;

	Product create(Integer id, Integer categoryId, String name, Double price, String description, Boolean active,
			String urlImage) throws DomainException;

}
