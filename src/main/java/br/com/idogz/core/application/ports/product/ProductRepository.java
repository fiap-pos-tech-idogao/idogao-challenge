package br.com.idogz.core.application.ports.product;

import java.util.List;
import br.com.idogz.core.domain.Product;
import br.com.idogz.core.domain.Name;
import br.com.idogz.core.domain.exception.DomainException;

public interface ProductRepository {
	
	Product create(Product product) throws DomainException;

	void update(Product product) throws DomainException;

	void remove(Integer idProduct) throws DomainException;
	
	Product findById(Integer idProduct) throws DomainException; 

	Product findByName(Name name) throws DomainException; 

	List<Product> findByCategoryId(Integer idCategory) throws DomainException; 

}
