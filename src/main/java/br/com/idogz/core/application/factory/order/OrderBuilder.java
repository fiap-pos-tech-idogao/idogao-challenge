package br.com.idogz.core.application.factory.order;

import java.time.LocalDate;

import br.com.idogz.core.domain.Order;
import br.com.idogz.core.domain.exception.DomainException;
import br.com.idogz.core.domain.exception.ValidationException;

public interface OrderBuilder {

	OrderBuilder client(final int clientId) throws DomainException;

	OrderBuilder date(final LocalDate date);

	OrderBuilder ticketOrder(final int ticketOrder) throws ValidationException;

	OrderBuilder status(final int idStatus);

	OrderBuilder notes(final String notes) throws ValidationException;

	OrderBuilder addProduct(final Integer productId, final int quantity, final int orderId, String notes)
			throws DomainException;

	Order build() throws DomainException;

	OrderBuilder addProduct(final int categoryId, final String categoryName, final String productName,
			final Double price, final String description, final Boolean active, final String urlImage,
			final int quantity, final String notes, final int productId) throws DomainException;

	OrderBuilder idOrder(final int orderId);

}
