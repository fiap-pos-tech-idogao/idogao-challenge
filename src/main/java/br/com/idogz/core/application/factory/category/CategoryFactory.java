package br.com.idogz.core.application.factory.category;

import br.com.idogz.core.domain.Category;
import br.com.idogz.core.domain.exception.DomainException;

public interface CategoryFactory {

	Category create(String name) throws DomainException;

	Category create(Integer id, String name) throws DomainException;

}
