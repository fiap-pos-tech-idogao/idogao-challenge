package br.com.idogz.core.application.factory.order;

import java.time.LocalDate;
import java.util.Objects;

import br.com.idogz.core.application.ports.client.ClientRepository;
import br.com.idogz.core.domain.Client;
import br.com.idogz.core.domain.Notes;
import br.com.idogz.core.domain.Order;
import br.com.idogz.core.domain.OrderProductsList;
import br.com.idogz.core.domain.StatusOrder;
import br.com.idogz.core.domain.TicketOrder;
import br.com.idogz.core.domain.exception.DomainException;
import br.com.idogz.core.domain.exception.ValidationException;

public class OrderBuilderImpl implements OrderBuilder {

	private static final int DEFAULT_CLIENT_ID = 1;
	private int orderId;
	private Client client;
	private LocalDate date = LocalDate.now();
	private TicketOrder ticketOrder = TicketOrder.ZERO;
	private StatusOrder status = StatusOrder.AWAITING_PAYMENT;
	private Notes notes = Notes.EMPTY;
	private OrderProductsList orderProducts = OrderProductsList.EMPTY;
	private final ClientRepository clientRepository;
	private final OrderProductFactory orderProductFactory;

	public OrderBuilderImpl(final ClientRepository clientRepository, final OrderProductFactory orderProductFactory) {
		this.clientRepository = clientRepository;
		this.orderProductFactory = orderProductFactory;
	}

	@Override
	public OrderBuilder idOrder(final int orderId) {
		this.orderId = orderId;
		return this;
	}

	@Override
	public OrderBuilder client(final int clientId) throws DomainException {
		if (clientId == 0) {
			client = clientRepository.find(OrderBuilderImpl.DEFAULT_CLIENT_ID);
		} else {
			client = clientRepository.find(clientId);
		}
		return this;
	}

	@Override
	public OrderBuilder date(final LocalDate date) {
		this.date = date;
		return this;
	}

	@Override
	public OrderBuilder ticketOrder(final int ticketOrder) throws ValidationException {
		this.ticketOrder = new TicketOrder(ticketOrder);
		return this;
	}

	@Override
	public OrderBuilder status(final int idStatus) {
		status = StatusOrder.findById(idStatus);
		return this;
	}

	@Override
	public OrderBuilder notes(final String notes) throws ValidationException {
		if (Objects.nonNull(notes) && !(notes.isEmpty() || notes.isBlank())) {
			this.notes = new Notes(notes);
		}
		return this;
	}

	@Override
	public OrderBuilder addProduct(final Integer productId, final int quantity, final int orderId, final String notes)
			throws DomainException {
		final var orderProduct = orderProductFactory.create(productId, quantity, notes, orderId);
		orderProducts = orderProducts.add(orderProduct);
		return this;
	}

	@Override
	public OrderBuilder addProduct(final int categoryId, final String categoryName, final String productName,
			final Double price, final String description, final Boolean active, final String urlImage,
			final int quantity, final String notes, final int productId) throws DomainException {
		final var orderProduct = orderProductFactory.create(categoryName, productName, price, description, active,
				urlImage, quantity, notes, 0, productId, categoryId);
		orderProducts = orderProducts.add(orderProduct);
		return this;
	}

	@Override
	public Order build() throws ValidationException {
		final var order = Order.create(date, ticketOrder, status, client, notes, orderProducts);
		order.updateIdWtihNotZero(orderId);
		reset();
		return order;
	}

	private void reset() {
		orderId = 0;
		client = null;
		date = LocalDate.now();
		ticketOrder = TicketOrder.ZERO;
		status = StatusOrder.AWAITING_PAYMENT;
		notes = Notes.EMPTY;
		orderProducts = OrderProductsList.EMPTY;
	}

}
