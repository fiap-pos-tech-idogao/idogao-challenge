package br.com.idogz.core.application.factory.order;

import br.com.idogz.core.domain.OrderProduct;
import br.com.idogz.core.domain.exception.DomainException;

public interface OrderProductFactory {

	OrderProduct create(final Integer productId, final int quantity, final String notes, final int orderId)
			throws DomainException;

	OrderProduct create(final String categoryName, final String productName, final Double price,
			final String description, final Boolean active, final String urlImage, final int quantity,
			final String notes, int orderId, int productId, int categoryId) throws DomainException;

}
