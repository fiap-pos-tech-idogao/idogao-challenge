package br.com.idogz.core.application.ports.order;

import java.util.List;

import br.com.idogz.core.domain.OrderProduct;
import br.com.idogz.core.domain.exception.DomainException;

public interface OrderProductRepository {

	void addAll(final List<OrderProduct> orderProducts) throws DomainException;

}
