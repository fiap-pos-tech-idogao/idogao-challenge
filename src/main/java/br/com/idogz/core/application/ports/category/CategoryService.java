package br.com.idogz.core.application.ports.category;

import br.com.idogz.core.domain.Category;
import br.com.idogz.core.domain.exception.DomainException;

public interface CategoryService {

	Category findById(Integer idCategory) throws DomainException;
}
