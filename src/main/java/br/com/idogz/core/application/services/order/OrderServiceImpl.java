package br.com.idogz.core.application.services.order;

import java.util.List;

import br.com.idogz.core.application.ports.order.OrderProductRepository;
import br.com.idogz.core.application.ports.order.OrderRepository;
import br.com.idogz.core.application.ports.order.OrderService;
import br.com.idogz.core.application.ports.transaction.TransactionManagerHelper;
import br.com.idogz.core.domain.Order;
import br.com.idogz.core.domain.exception.DomainException;

public class OrderServiceImpl implements OrderService {

	private final OrderRepository orderRepository;
	private final OrderProductRepository orderProductRepository;
	private final TransactionManagerHelper transactionManager;

	public OrderServiceImpl(final OrderRepository orderRepository, final OrderProductRepository orderProductRepository,
			final TransactionManagerHelper transactionManager) {
		this.orderRepository = orderRepository;
		this.orderProductRepository = orderProductRepository;
		this.transactionManager = transactionManager;
	}

	@Override
	public Order create(final Order order) throws DomainException {
		transactionManager.startTransaction("createOrder");
		saveOrder(order);
		transactionManager.commit();
		return order;
	}

	private void saveOrder(final Order order) throws DomainException {
		try {
			orderRepository.add(order);
			orderProductRepository.addAll(order.products());
		} catch (final DomainException e) {
			transactionManager.rollback();
			throw e;
		}
	}

	@Override
	public List<Order> allOrders() throws DomainException {
		return orderRepository.allOrders();
	}

	@Override
	public Order findById(final int id) throws DomainException {
		return orderRepository.orderWithId(id);
	}

}
