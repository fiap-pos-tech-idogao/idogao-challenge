package br.com.idogz.core.application.factory.client;

import br.com.idogz.core.domain.Client;
import br.com.idogz.core.domain.Cpf;
import br.com.idogz.core.domain.Email;
import br.com.idogz.core.domain.Name;
import br.com.idogz.core.domain.exception.DomainException;

public class ClientFactoryImpl implements ClientFactory {

	@Override
	public Client create(String name, String cpf, String email) throws DomainException {
		final var nameVO = new Name(name);
		final var cpfVO = new Cpf(cpf);
		final var emailVO = new Email(email);
		return new Client(nameVO, cpfVO, emailVO);
	}

	@Override
	public Client create(Integer id, String name, String cpf, String email) throws DomainException {
		final var nameVO = new Name(name);
		final var cpfVO = new Cpf(cpf);
		final var emailVO = new Email(email);
		final var client = new Client(nameVO, cpfVO, emailVO);
		client.updateId(id);
		return client;
	}

}
