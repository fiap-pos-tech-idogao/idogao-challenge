package br.com.idogz.core.application.services.client;

import br.com.idogz.core.application.ports.client.ClientRepository;
import br.com.idogz.core.application.ports.client.ClientService;
import br.com.idogz.core.domain.Client;
import br.com.idogz.core.domain.Cpf;
import br.com.idogz.core.domain.exception.DomainException;

public class ClientServiceImpl implements ClientService {

	private final ClientRepository repository;

	public ClientServiceImpl(final ClientRepository repository) {
		this.repository = repository;
	}

	@Override
	public Client create(final Client client) throws DomainException {
		return repository.create(client);
	}

	@Override
	public Client find(final Cpf cpf) throws DomainException {
		return repository.find(cpf);
	}

	@Override
	public Client find(final int id) throws DomainException {
		return repository.find(id);
	}

}
