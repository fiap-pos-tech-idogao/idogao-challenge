package br.com.idogz.core.application.factory.client;

import br.com.idogz.core.domain.Client;
import br.com.idogz.core.domain.exception.DomainException;

public interface ClientFactory {

	Client create(String name, String cpf, String email) throws DomainException;
	
	Client create(Integer id, String name, String cpf, String email) throws DomainException;
	
}
