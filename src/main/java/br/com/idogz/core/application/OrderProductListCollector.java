package br.com.idogz.core.application;

import java.util.ArrayList;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import br.com.idogz.core.domain.OrderProduct;
import br.com.idogz.core.domain.OrderProductsList;
import br.com.idogz.core.domain.exception.ValidationException;

public class OrderProductListCollector implements Collector<OrderProduct, ArrayList<OrderProduct>, OrderProductsList> {

	@Override
	public Supplier<ArrayList<OrderProduct>> supplier() {
		return ArrayList::new;
	}

	@Override
	public BiConsumer<ArrayList<OrderProduct>, OrderProduct> accumulator() {
		return ArrayList::add;
	}

	@Override
	public BinaryOperator<ArrayList<OrderProduct>> combiner() {
		return (final var left, final var right) -> {
			left.addAll(right);
			return left;
		};
	}

	@Override
	public Function<ArrayList<OrderProduct>, OrderProductsList> finisher() {
		return (final var t) -> {
			try {
				return new OrderProductsList(t);
			} catch (final ValidationException e) {
				e.printStackTrace();
				return null;
			}
		};
	}

	@Override
	public Set<Characteristics> characteristics() {
		return Set.of(Characteristics.UNORDERED);
	}

}
