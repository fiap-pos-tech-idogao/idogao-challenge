package br.com.idogz.core.application.ports.transaction;

import org.springframework.transaction.TransactionException;

public interface TransactionManagerHelper {

	void rollback() throws TransactionException;

	void commit() throws TransactionException;

	void startTransaction(final String transactionName) throws TransactionException;

}
