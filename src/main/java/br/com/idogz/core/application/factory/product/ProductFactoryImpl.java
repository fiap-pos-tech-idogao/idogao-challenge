package br.com.idogz.core.application.factory.product;

import br.com.idogz.core.application.ports.category.CategoryRepository;
import br.com.idogz.core.domain.Name;
import br.com.idogz.core.domain.Product;
import br.com.idogz.core.domain.exception.DomainException;

public class ProductFactoryImpl implements ProductFactory {

	private final CategoryRepository categoryRepository;

	public ProductFactoryImpl(final CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	@Override
	public Product create(final Integer idCategory, final String name, final Double price, final String description,
			final Boolean active, final String urlImage) throws DomainException {
		final var nameVO = new Name(name);
		final var category = categoryRepository.findById(idCategory);
		return new Product(category, nameVO, price, description, active, urlImage);
	}

	@Override
	public Product create(final Integer id, final Integer idCategory, final String name, final Double price,
			final String description, final Boolean active, final String urlImage) throws DomainException {
		final var nameVO = new Name(name);
		final var category = categoryRepository.findById(idCategory);
		final var product = new Product(category, nameVO, price, description, active, urlImage);
		product.updateId(id);
		return product;
	}

}
