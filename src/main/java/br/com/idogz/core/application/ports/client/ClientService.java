package br.com.idogz.core.application.ports.client;

import br.com.idogz.core.domain.Client;
import br.com.idogz.core.domain.Cpf;
import br.com.idogz.core.domain.exception.DomainException;

public interface ClientService {

	Client create(Client client) throws DomainException;

	Client find(Cpf cpf) throws DomainException;

	Client find(int id) throws DomainException;

}
