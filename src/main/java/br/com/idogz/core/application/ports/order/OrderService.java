package br.com.idogz.core.application.ports.order;

import java.util.List;

import br.com.idogz.core.domain.Order;
import br.com.idogz.core.domain.exception.DomainException;

public interface OrderService {

	Order create(Order order) throws DomainException;

	List<Order> allOrders() throws DomainException;

	Order findById(int id) throws DomainException;

}
