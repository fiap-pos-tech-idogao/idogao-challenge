package br.com.idogz.core.application.services.product;

import java.util.List;

import br.com.idogz.core.application.ports.product.ProductRepository;
import br.com.idogz.core.application.ports.product.ProductService;
import br.com.idogz.core.domain.Product;
import br.com.idogz.core.domain.Name;
import br.com.idogz.core.domain.exception.DomainException;

public class ProductServiceImpl implements ProductService {

	private final ProductRepository repository;
	
	public ProductServiceImpl(ProductRepository repository) {
		this.repository = repository;
	}

	@Override
	public Product create(Product product) throws DomainException {
		return repository.create(product);
	}

	@Override
	public void update(Product product) throws DomainException {
		repository.update(product);
	}

	@Override
	public void remove(Integer idProduct) throws DomainException {
		repository.remove(idProduct);
	}

	@Override
	public Product findById(Integer idProduct) throws DomainException {
		return repository.findById(idProduct);
	}

	@Override
	public Product findByName(Name name) throws DomainException {
		return repository.findByName(name);
	}

	@Override
	public List<Product> findByCategoryId(Integer idCategory) throws DomainException {
		return repository.findByCategoryId(idCategory);
	}

}
