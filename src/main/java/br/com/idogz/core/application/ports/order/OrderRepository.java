package br.com.idogz.core.application.ports.order;

import java.util.List;

import br.com.idogz.core.domain.Order;
import br.com.idogz.core.domain.exception.DomainException;

public interface OrderRepository {

	void add(Order order) throws DomainException;

	List<Order> allOrders() throws DomainException;

	Order orderWithId(int id) throws DomainException;

}
