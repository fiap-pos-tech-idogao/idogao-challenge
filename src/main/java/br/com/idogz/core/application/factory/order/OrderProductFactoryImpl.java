package br.com.idogz.core.application.factory.order;

import java.util.Objects;

import br.com.idogz.core.application.ports.product.ProductRepository;
import br.com.idogz.core.domain.Category;
import br.com.idogz.core.domain.Name;
import br.com.idogz.core.domain.Notes;
import br.com.idogz.core.domain.OrderProduct;
import br.com.idogz.core.domain.Product;
import br.com.idogz.core.domain.exception.DomainException;
import br.com.idogz.core.domain.exception.ValidationException;

public class OrderProductFactoryImpl implements OrderProductFactory {

	private final ProductRepository productRepository;

	public OrderProductFactoryImpl(final ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Override
	public OrderProduct create(final Integer productId, final int quantity, final String notes, final int orderId)
			throws DomainException {
		final var notesVO = OrderProductFactoryImpl.toNotes(notes);
		final var product = productRepository.findById(productId);
		return new OrderProduct(orderId, product, quantity, notesVO);
	}

	@Override
	public OrderProduct create(final String categoryName, final String productName, final Double price,
			final String description, final Boolean active, final String urlImage, final int quantity,
			final String notes, final int orderId, final int productId, final int categoryId) throws DomainException {
		final var categoryVo = OrderProductFactoryImpl.createCategory(categoryName, categoryId);
		final var product = OrderProductFactoryImpl.createProduct(productName, price, description, active, urlImage,
				productId, categoryVo);
		final var notesVo = OrderProductFactoryImpl.createNotes(notes);
		return new OrderProduct(orderId, product, quantity, notesVo);
	}

	private static Category createCategory(final String categoryName, final Integer categoryId)
			throws DomainException {
		final var categoryNameVo = new Name(categoryName);
		final var categoryVo = new Category(categoryNameVo);
		categoryVo.updateId(categoryId);
		return categoryVo;
	}

	private static Product createProduct(final String productName, final Double price, final String description,
			final Boolean active, final String urlImage, final int productId, final Category categoryVo)
					throws DomainException {
		final var productNameVo = new Name(productName);
		final var product = new Product(categoryVo, productNameVo, price, description, active, urlImage);
		product.updateId(productId);
		return product;
	}

	private static Notes createNotes(final String notes) throws ValidationException {
		if (Objects.nonNull(notes) && !(notes.isEmpty() || notes.isBlank())) {
			return new Notes(notes);
		}
		return Notes.EMPTY;
	}

	private static Notes toNotes(final String notes) throws ValidationException {
		if (Objects.nonNull(notes) && !(notes.isEmpty() || notes.isBlank())) {
			return new Notes(notes);
		}
		return Notes.EMPTY;
	}
}
