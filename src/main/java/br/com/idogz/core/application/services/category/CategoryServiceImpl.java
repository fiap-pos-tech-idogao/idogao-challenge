package br.com.idogz.core.application.services.category;

import br.com.idogz.core.application.ports.category.CategoryRepository;
import br.com.idogz.core.application.ports.category.CategoryService;
import br.com.idogz.core.domain.Category;
import br.com.idogz.core.domain.exception.DomainException;

public class CategoryServiceImpl implements CategoryService {

	private final CategoryRepository repository;

	public CategoryServiceImpl(final CategoryRepository repository) {
		this.repository = repository;
	}

	@Override
	public Category findById(final Integer idCategory) throws DomainException {
		return repository.findById(idCategory);
	}
}
