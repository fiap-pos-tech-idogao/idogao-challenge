package br.com.idogz.core.domain;

import java.util.Objects;

import br.com.idogz.core.domain.exception.InvalidClientException;
import br.com.idogz.core.domain.exception.ValidationException;

public class Client extends AssertionConcern implements Entity<Client> {

	private static final String CLIENT_NAME_CANNOT_BE_NULL = "Client name cannot be null";
	private static final String CLIENT_EMAIL_CANNOT_BE_NULL = "Client email cannot be null";
	private static final String CLIENT_CPF_CANNOT_BE_NULL = "Client CPF cannot be null";
	private Integer id;
	private Name name;
	private Cpf cpf;
	private Email email;

	public Client(final Name name, final Cpf cpf, final Email email) throws ValidationException {
		setName(name);
		setCpf(cpf);
		setEmail(email);
	}

	public String emailAddress() {
		return email.getAddress();
	}

	public String formattingCpf() {
		return cpf.value();
	}

	public Integer id() {
		return id;
	}

	public String name() {
		return name.value();
	}

	public String unformattingCpf() {
		return cpf.unformattingValue();
	}

	private void setCpf(final Cpf cpf) throws ValidationException {
		assertNotNull(cpf, new InvalidClientException(CLIENT_CPF_CANNOT_BE_NULL));
		this.cpf = cpf;
	}

	private void setEmail(final Email email) throws ValidationException {
		assertNotNull(email, new InvalidClientException(CLIENT_EMAIL_CANNOT_BE_NULL));
		this.email = email;
	}

	public void updateId(final Integer id) {
		this.id = id;
	}

	private void setName(final Name name) throws ValidationException {
		assertNotNull(name, new InvalidClientException(CLIENT_NAME_CANNOT_BE_NULL));
		this.name = name;
	}

	@Override
	public boolean sameIdentityAs(final Client other) {
		return Objects.nonNull(other) && cpf.equals(other.cpf);
	}

	@Override
	public int hashCode() {
		return Objects.hash(cpf, email, id, name);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (Client) obj;
		return sameIdentityAs(other);
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", name=" + name + ", cpf=" + cpf + ", email=" + email + "]";
	}

}
