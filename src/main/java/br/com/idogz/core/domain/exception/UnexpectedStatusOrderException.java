package br.com.idogz.core.domain.exception;

public class UnexpectedStatusOrderException extends RuntimeException{

	private static final String UNEXPECTED_STATUS_ORDER = "Unexpected Status Order";
	/**
	 *
	 */
	private static final long serialVersionUID = 1792828387607484440L;

	public UnexpectedStatusOrderException() {
		super(UNEXPECTED_STATUS_ORDER);
	}

}
