package br.com.idogz.core.domain;

import java.util.Objects;
import java.util.regex.Pattern;

import br.com.idogz.core.domain.exception.DomainException;
import br.com.idogz.core.domain.exception.InvalidCpfException;
import br.com.idogz.core.domain.exception.ValidationException;

public class Cpf extends AssertionConcern implements ValueObject<Cpf> {

	private static final String MUST_BE_A_VALID_CPF = "Must be a valid CPF";
	private static final String CPF_CANNOT_BE_EMPTY_OR_BLANK = "CPF cannot be empty or blank";
	private static final String CPF_CANNOT_BE_NULL = "CPF cannot be null";
	private static final Pattern NOT_DIGITS = Pattern.compile("\\D");
	private static final Pattern CPF_DIGITS_SPLIT = Pattern.compile("(\\d{3})(\\d{3})(\\d{3})(\\d{2})");
	private static final String CPF_PATTERN = "$1.$2.$3-$4";
	private static final String EMPTY_STRING = "";
	/**
	 *
	 */
	private static final long serialVersionUID = 936255562603341914L;
	private String value;

	public Cpf(final String value) throws DomainException {
		setValue(value);
	}

	public String unformattingValue() {
		return value.replace(".", Cpf.EMPTY_STRING).replace("-", Cpf.EMPTY_STRING);
	}

	public String value() {
		return value;
	}

	private void setValue(String value) throws ValidationException {
		if (Objects.nonNull(value)) {
			value = value.replaceAll(Cpf.NOT_DIGITS.pattern(), Cpf.EMPTY_STRING);
			value = value.replaceAll(Cpf.CPF_DIGITS_SPLIT.pattern(), Cpf.CPF_PATTERN);
		}
		assertNotNull(value, new InvalidCpfException(Cpf.CPF_CANNOT_BE_NULL));
		assertNotEmptyOrBlank(value, new InvalidCpfException(Cpf.CPF_CANNOT_BE_EMPTY_OR_BLANK));
		assertTrue(Cpf.isCpf(value), new InvalidCpfException(Cpf.MUST_BE_A_VALID_CPF));
		this.value = value;
	}

	private static boolean isCpf(String cpf) {
		var d1 = 0;
		var d2 = 0;
		var digito1 = 0;
		var digito2 = 0;
		var resto = 0;
		var digitoCPF = 0;

		cpf = cpf.replace(".", Cpf.EMPTY_STRING);
		cpf = cpf.replace("-", Cpf.EMPTY_STRING);

		try {
			Long.parseLong(cpf);
		} catch (final NumberFormatException e) {
			return false;
		}

		if (Long.parseLong(cpf) == 0) {
			return false;
		}

		for (var nCount = 1; nCount < cpf.length() - 1; nCount++) {
			digitoCPF = Integer.parseInt(cpf.substring(nCount - 1, nCount));
			d1 = d1 + (11 - nCount) * digitoCPF;
			d2 = d2 + (12 - nCount) * digitoCPF;
		}

		resto = d1 % 11;

		if (resto < 2) {
			digito1 = 0;
		} else {
			digito1 = 11 - resto;
		}

		d2 += 2 * digito1;

		resto = d2 % 11;

		if (resto < 2) {
			digito2 = 0;
		} else {
			digito2 = 11 - resto;
		}

		final var nDigVerific = cpf.substring(cpf.length() - 2, cpf.length());

		final var nDigResult = String.valueOf(digito1) + String.valueOf(digito2);

		return nDigVerific.equals(nDigResult);
	}

	@Override
	public boolean sameValueAs(final Cpf other) {
		return Objects.nonNull(other) && value.equals(other.value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (Cpf) obj;
		return sameValueAs(other);
	}

	@Override
	public String toString() {
		return value;
	}

}
