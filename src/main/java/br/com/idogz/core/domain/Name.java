package br.com.idogz.core.domain;

import java.util.Objects;

import br.com.idogz.core.domain.exception.DomainException;
import br.com.idogz.core.domain.exception.InvalidNameException;
import br.com.idogz.core.domain.exception.ValidationException;

public class Name extends AssertionConcern implements ValueObject<Name> {

	private static final String NAME_CANNOT_BE_EMPTY_OR_BLANK = "Name cannot be empty or blank";

	private static final String NAME_CANNOT_BE_NULL = "Name cannot be null";

	/**
	 *
	 */
	private static final long serialVersionUID = 2986671169106201357L;

	private String value;

	public Name(final String value) throws DomainException {
		setValue(value);
	}

	private void setValue(final String value) throws ValidationException {
		assertNotNull(value, new InvalidNameException(NAME_CANNOT_BE_NULL));
		assertNotEmptyOrBlank(value, new InvalidNameException(NAME_CANNOT_BE_EMPTY_OR_BLANK));
		this.value = value;
	}

	public String value() {
		return value;
	}

	@Override
	public boolean sameValueAs(final Name other) {
		return Objects.nonNull(other) && value.equals(other.value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (Name) obj;
		return sameValueAs(other);
	}

	@Override
	public String toString() {
		return value;
	}

}
