package br.com.idogz.core.domain.exception;

public class InvalidOrderException extends ValidationException {

	private static final String INVALID_ORDER = "Invalid order: ";
	/**
	 *
	 */
	private static final long serialVersionUID = -1093137066176337099L;

	public InvalidOrderException(final String message) {
		super(INVALID_ORDER + message);
	}

}
