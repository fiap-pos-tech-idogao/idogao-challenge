package br.com.idogz.core.domain;

import java.util.Objects;

import br.com.idogz.core.domain.exception.InvalidNotesException;
import br.com.idogz.core.domain.exception.ValidationException;

public class Notes extends AssertionConcern implements ValueObject<Notes> {

	private static final String CANNOT_BE_EMPTY_OR_BLANK = "cannot be empty or blank";
	private static final String CANNOT_BE_NULL = "cannot be null";
	/**
	 *
	 */
	private static final long serialVersionUID = -649533263802402582L;
	public static final Notes EMPTY = new Notes();
	private String value;

	private Notes() {
		value = "";
	}

	public Notes(final String value) throws ValidationException {
		setValue(value);
	}

	private void setValue(final String value) throws ValidationException {
		assertNotNull(value, new InvalidNotesException(CANNOT_BE_NULL));
		assertNotEmptyOrBlank(value, new InvalidNotesException(CANNOT_BE_EMPTY_OR_BLANK));
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public boolean sameValueAs(final Notes other) {
		return Objects.nonNull(other) && value.equals(other.value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (Notes) obj;
		return sameValueAs(other);
	}

	@Override
	public String toString() {
		return value;
	}

}
