package br.com.idogz.core.domain.exception;

public class InvalidNameException extends ValidationException {

	private static final String INVALID_NAME = "Invalid name: ";
	/**
	 *
	 */
	private static final long serialVersionUID = -8670545258769678415L;

	public InvalidNameException(final String message) {
		super(INVALID_NAME + message);
	}

}
