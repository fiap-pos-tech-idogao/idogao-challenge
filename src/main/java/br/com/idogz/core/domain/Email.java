package br.com.idogz.core.domain;

import java.util.Objects;

import br.com.idogz.core.domain.exception.InvalidEmailException;
import br.com.idogz.core.domain.exception.ValidationException;

public class Email extends AssertionConcern implements ValueObject<Email> {

	private static final String EMAIL_MUST_BE_VALID = "Email must be valid";
	private static final String VALID_EMAIL_PATTERN = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
	private static final String EMAIL_CANNOT_BE_EMPTY_OR_BLANK = "Email cannot be empty or blank";
	private static final String EMAIL_CANNOT_BE_NULL = "Email cannot be null";
	/**
	 *
	 */
	private static final long serialVersionUID = -1061428686323864948L;
	private String value;

	public Email(final String value) throws ValidationException {
		setAddress(value);
	}

	private void setAddress(final String value) throws ValidationException {
		assertNotNull(value, new InvalidEmailException(Email.EMAIL_CANNOT_BE_NULL));
		assertNotEmptyOrBlank(value, new InvalidEmailException(Email.EMAIL_CANNOT_BE_EMPTY_OR_BLANK));
		assertMatchesPattern(value, Email.VALID_EMAIL_PATTERN,
				new InvalidEmailException(Email.EMAIL_MUST_BE_VALID));
		this.value = value;
	}

	public String getAddress() {
		return value;
	}

	@Override
	public boolean sameValueAs(final Email other) {
		return Objects.nonNull(other) && value.equals(other.value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (Email) obj;
		return sameValueAs(other);
	}

	@Override
	public String toString() {
		return value;
	}

}
