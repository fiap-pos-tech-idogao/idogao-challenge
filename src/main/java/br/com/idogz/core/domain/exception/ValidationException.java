package br.com.idogz.core.domain.exception;

public class ValidationException extends DomainException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8895828432311052920L;

	public ValidationException(String message) {
		super(message);
	}

	public ValidationException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
