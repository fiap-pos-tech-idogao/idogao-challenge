package br.com.idogz.core.domain;

public interface Entity<T> {

	boolean sameIdentityAs(T other);

}
