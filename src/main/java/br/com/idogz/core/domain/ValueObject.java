package br.com.idogz.core.domain;

import java.io.Serializable;

public interface ValueObject<T> extends Serializable {

	boolean sameValueAs(T other);

}
