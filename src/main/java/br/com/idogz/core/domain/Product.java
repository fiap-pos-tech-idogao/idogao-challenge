package br.com.idogz.core.domain;

import java.util.Objects;

import br.com.idogz.core.domain.exception.InvalidProductException;
import br.com.idogz.core.domain.exception.ValidationException;

public class Product extends AssertionConcern implements Entity<Product> {

	private static final String ACTIVE_PRODUCT_CANNOT_BE_NULL = "active product cannot be null";
	private static final String DESCRIPTION_PRODUCT_CANNOT_BE_NULL = "description product cannot be null";
	private static final String PRICE_PRODUCT_CANNOT_BE_NULL = "price product cannot be null";
	private static final String NAME_PRODUCT_CANNOT_BE_NULL = "name product cannot be null";
	private static final String CATEGORY_PRODUCT_CANNOT_BE_NULL = "category product cannot be null";
	private Integer id;
	private Category category;
	private Name name;
	private Double price;
	private String description;
	private Boolean active;
	private String urlImage;

	public Product(final Category category, final Name name, final Double price, final String description,
			final Boolean active, final String urlImage) throws ValidationException {
		setCategory(category);
		setName(name);
		setPrice(price);
		setDescription(description);
		setActive(active);
		setUrlImage(urlImage);
	}

	public static Product create(final Category category, final Name name, final Double price, final String description,
			final Boolean active, final String urlImage) throws ValidationException {
		return new Product(category, name, price, description, active, urlImage);
	}

	public Integer id() {
		return id;
	}

	public void updateId(final Integer id) {
		this.id = id;
	}

	public Category category() {
		return category;
	}

	private void setCategory(final Category category) throws ValidationException {
		assertNotNull(category, new InvalidProductException(Product.CATEGORY_PRODUCT_CANNOT_BE_NULL));
		this.category = category;
	}

	public String name() {
		return name.value();
	}

	private void setName(final Name name) throws ValidationException {
		assertNotNull(name, new InvalidProductException(Product.NAME_PRODUCT_CANNOT_BE_NULL));
		this.name = name;
	}

	public Double price() {
		return price;
	}

	private void setPrice(final Double price) throws ValidationException {
		assertNotNull(price, new InvalidProductException(Product.PRICE_PRODUCT_CANNOT_BE_NULL));
		this.price = price;
	}

	public String description() {
		return description;
	}

	private void setDescription(final String description) throws ValidationException {
		assertNotNull(description, new InvalidProductException(Product.DESCRIPTION_PRODUCT_CANNOT_BE_NULL));
		this.description = description;
	}

	public Boolean active() {
		return active;
	}

	private void setActive(final Boolean active) throws ValidationException {
		assertNotNull(active, new InvalidProductException(Product.ACTIVE_PRODUCT_CANNOT_BE_NULL));
		this.active = active;
	}

	@Override
	public boolean sameIdentityAs(final Product product) {
		return Objects.nonNull(product) && id.equals(product.id);
	}

	public String urlImage() {
		return urlImage;
	}

	private void setUrlImage(final String urlImage) {
		this.urlImage = urlImage;
	}
}
