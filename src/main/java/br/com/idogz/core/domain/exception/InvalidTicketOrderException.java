package br.com.idogz.core.domain.exception;

public class InvalidTicketOrderException extends ValidationException {

	private static final String INVALID_TICKET_ORDER = "Invalid ticket order: ";
	/**
	 *
	 */
	private static final long serialVersionUID = 6984634479177507655L;

	public InvalidTicketOrderException(final String message) {
		super(INVALID_TICKET_ORDER + message);
	}

}
