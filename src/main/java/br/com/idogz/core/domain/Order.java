package br.com.idogz.core.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import br.com.idogz.core.domain.exception.InvalidOrderException;
import br.com.idogz.core.domain.exception.ValidationException;

public class Order extends AssertionConcern implements Entity<Order> {

	private static final String PRODUCT_ORDER_LIST_CANNOT_BE_NULL = "product order list cannot be null";
	private static final String CLIENT_ORDER_NOTES_CANNOT_BE_NULL = "client order notes cannot be null";
	private static final String CLIENT_ORDER_CANNOT_BE_NULL = "client order cannot be null";
	private static final String ORDER_STATUS_CANNOT_BE_NULL = "order status cannot be null";
	private static final String TICKET_ORDER_CANNOT_BE_NULL = "ticket order cannot be null";
	private static final String ORDER_DATE_CANNOT_BE_NULL = "order date cannot be null";
	private int id;
	private LocalDate date;
	private TicketOrder ticketOrder;
	private StatusOrder status;
	private Client client;
	private Notes notesOrder;
	private OrderProductsList orderProducts;

	private Order(final LocalDate date, final TicketOrder ticketOrder, final StatusOrder status, final Client client,
			final Notes notesOrder, final OrderProductsList orderProducts) throws ValidationException {
		setDate(date);
		setTicketOrder(ticketOrder);
		setStatus(status);
		setClient(client);
		setNotesOrder(notesOrder);
		setOrderProducts(orderProducts);
	}

	public static Order create(final LocalDate date, final TicketOrder ticketOrder, final StatusOrder status,
			final Client client, final Notes notesOrder, final OrderProductsList orderProducts)
					throws ValidationException {
		return new Order(date, ticketOrder, status, client, notesOrder, orderProducts);
	}

	private void setDate(final LocalDate date) throws ValidationException {
		assertNotNull(date, new InvalidOrderException(Order.ORDER_DATE_CANNOT_BE_NULL));
		this.date = date;
	}

	private void setTicketOrder(final TicketOrder ticketOrder) throws ValidationException {
		assertNotNull(ticketOrder, new InvalidOrderException(Order.TICKET_ORDER_CANNOT_BE_NULL));
		this.ticketOrder = ticketOrder;
	}

	private void setStatus(final StatusOrder status) throws ValidationException {
		assertNotNull(status, new InvalidOrderException(Order.ORDER_STATUS_CANNOT_BE_NULL));
		this.status = status;
	}

	private void setClient(final Client client) throws ValidationException {
		assertNotNull(client, new InvalidOrderException(Order.CLIENT_ORDER_CANNOT_BE_NULL));
		this.client = client;
	}

	private void setNotesOrder(final Notes notesOrder) throws ValidationException {
		assertNotNull(notesOrder, new InvalidOrderException(Order.CLIENT_ORDER_NOTES_CANNOT_BE_NULL));
		this.notesOrder = notesOrder;
	}

	private void setOrderProducts(final OrderProductsList orderProducts) throws ValidationException {
		assertNotNull(orderProducts, new InvalidOrderException(Order.PRODUCT_ORDER_LIST_CANNOT_BE_NULL));
		this.orderProducts = orderProducts;
	}

	public StatusOrder status() {
		return status;
	}

	public Notes notes() {
		return notesOrder;
	}

	public TicketOrder ticket() {
		return ticketOrder;
	}

	public Client client() {
		return client;
	}

	public LocalDate date() {
		return date;
	}

	public void updateIdWtihNotZero(final int id) throws ValidationException {
		if (id != 0) {
			this.id = id;
			orderProducts.updateOrderId(this.id);
		}
	}

	public int id() {
		return id;
	}

	public List<OrderProduct> products() {
		return orderProducts.getProducts();
	}

	public BigDecimal totalValue() {
		return orderProducts.getProducts().parallelStream().map(OrderProduct::totalPrice).reduce(BigDecimal.ZERO,
				BigDecimal::add);
	}

	public void addProduct(final OrderProduct orderProduct) throws ValidationException {
		orderProducts = orderProducts.add(orderProduct);
	}

	public void addProducts(final List<OrderProduct> orderProducts) throws ValidationException {
		this.orderProducts = this.orderProducts.addAll(orderProducts);
	}

	public void updateTicket(final int ticketOrder) throws ValidationException {
		final var tickerOrderVO = new TicketOrder(ticketOrder);
		setTicketOrder(tickerOrderVO);
	}

	public void updateClient(final Client client) throws ValidationException {
		setClient(client);
	}

	@Override
	public boolean sameIdentityAs(final Order other) {
		return Objects.nonNull(other) && id == other.id;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", date=" + date + ", ticketOrder=" + ticketOrder + ", status=" + status
				+ ", client=" + client + ", notesOrder=" + notesOrder + ", orderProducts=" + orderProducts + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(client, date, id, notesOrder, orderProducts, status, ticketOrder);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (Order) obj;
		return Objects.equals(client, other.client) && Objects.equals(date, other.date) && id == other.id
				&& Objects.equals(notesOrder, other.notesOrder) && Objects.equals(orderProducts, other.orderProducts)
				&& status == other.status && Objects.equals(ticketOrder, other.ticketOrder);
	}

}
