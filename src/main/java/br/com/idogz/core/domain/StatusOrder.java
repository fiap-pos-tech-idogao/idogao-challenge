package br.com.idogz.core.domain;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

import br.com.idogz.core.domain.exception.UnexpectedStatusOrderException;

public enum StatusOrder implements ValueObject<StatusOrder> {

	AWAITING_PAYMENT(1, "Awaiting Payment"), RECEIVED(2, "Received"), IN_PREPARATION(3, "In Preparation"),
	READY(4, "Ready"), FINISHED(5, "Finished");

	private final int id;
	private final String description;

	StatusOrder(final int id, final String description) {
		this.id = id;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public static StatusOrder findById(final int id) {
		return Stream.of(StatusOrder.values()).filter(StatusOrder.statusOrderById(id)).findFirst()
				.orElseThrow(UnexpectedStatusOrderException::new);
	}

	private static Predicate<? super StatusOrder> statusOrderById(final int id) {
		return statusOrder -> statusOrder.getId() == id;
	}

	@Override
	public boolean sameValueAs(final StatusOrder other) {
		return Objects.nonNull(other) && equals(other);
	}

}
