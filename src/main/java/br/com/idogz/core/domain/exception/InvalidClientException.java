package br.com.idogz.core.domain.exception;

public class InvalidClientException extends ValidationException {

	private static final String INVALID_CLIENT = "Invalid client: ";
	/**
	 *
	 */
	private static final long serialVersionUID = -6955001952768956877L;

	public InvalidClientException(final String message) {
		super(InvalidClientException.INVALID_CLIENT + message);
	}

	public InvalidClientException(final String message, final Throwable cause) {
		super(InvalidClientException.INVALID_CLIENT + message, cause);
	}

}
