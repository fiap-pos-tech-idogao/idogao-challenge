package br.com.idogz.core.domain.exception;

public class InvalidNotesException extends ValidationException {

	private static final String INVALID_NOTES = "Invalid notes: ";
	/**
	 *
	 */
	private static final long serialVersionUID = -8378900480126290973L;

	public InvalidNotesException(final String message) {
		super(INVALID_NOTES + message);
	}

}
