package br.com.idogz.core.domain;

import java.util.Objects;

import br.com.idogz.core.domain.exception.InvalidProductException;
import br.com.idogz.core.domain.exception.ValidationException;

public class Category extends AssertionConcern implements Entity<Category> {

	private static final String NAME_CATEGORY_CANNOT_BE_NULL = "Category name cannot be null";
	private Integer id;
	private Name name;

	public Category(final Name name) throws ValidationException {
		setName(name);
	}

	public Integer id() {
		return id;
	}

	public void updateId(final Integer id) {
		this.id = id;
	}

	public String name() {
		return name.value();
	}

	private void setName(final Name name) throws ValidationException {
		assertNotNull(name, new InvalidProductException(Category.NAME_CATEGORY_CANNOT_BE_NULL));
		this.name = name;
	}

	@Override
	public boolean sameIdentityAs(final Category category) {
		return Objects.nonNull(category) && id.equals(category.id);
	}
}
