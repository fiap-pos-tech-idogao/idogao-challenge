package br.com.idogz.core.domain;

import java.util.Objects;
import java.util.regex.Pattern;

import br.com.idogz.core.domain.exception.ValidationException;

public class AssertionConcern {

	protected void assertNotNull(final Object object, final ValidationException exception) throws ValidationException {
		if (Objects.isNull(object)) {
			throw exception;
		}
	}

	protected void assertNotEmptyOrBlank(final String value, final ValidationException exception)
			throws ValidationException {
		if (value.isEmpty() || value.isBlank()) {
			throw exception;
		}
	}

	protected void assertFalse(final boolean value, final ValidationException exception) throws ValidationException {
		if (value) {
			throw exception;
		}
	}

	protected void assertTrue(final boolean value, final ValidationException exception) throws ValidationException {
		if (!value) {
			throw exception;
		}
	}

	protected void assertMatchesPattern(final String value, final String regex, final ValidationException exception)
			throws ValidationException {
		final var pattern = Pattern.compile(regex);
		final var matcher = pattern.matcher(value);
		if (!matcher.find()) {
			throw exception;
		}
	}

}
