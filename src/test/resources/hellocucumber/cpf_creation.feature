Feature: CPF Class Creation

  Scenario: Creating a new CPF object
    Given I have a CPF number "148.036.300-62"
    When I create a CPF object
    Then the CPF object should be created successfully
    
  Scenario: Creating a CPF object with an invalid number
    Given I have an invalid CPF number "000.000.000-00"
    When I attempt to create a CPF object
    Then the creation should fail with a message "Must be a valid cpf"

  Scenario: Creating a CPF object with an invalid CPF number format
    Given I have an invalid CPF number "123.456.789-00"
    When I attempt to create a CPF object
    Then the creation should fail with a message "Must be a valid cpf"
    
  Scenario: Creating a CPF object with an invalid CPF number format
    Given I have an invalid CPF number "A23.456.789-00"
    When I attempt to create a CPF object
    Then the creation should fail with a message "Must be a valid cpf"

  Scenario: Creating a CPF object without a number
    Given I don't have a CPF number
    When I attempt to create a CPF object
    Then the creation should fail with a message "Cpf cannot be null"
    
  Scenario: Creating a CPF object with a blank number
    Given I have a CPF number " "
    When I attempt to create a CPF object
    Then the creation should fail with a message "Cpf cannot be empty or blank"
    
  Scenario: Creating a CPF object with a empty number
    Given I have a CPF number ""
    When I attempt to create a CPF object
    Then the creation should fail with a message "Cpf cannot be empty or blank"

  Scenario: Creating a CPF object with a CNPJ number
    Given I have a CNPJ number "12.345.678/0001-90"
    When I attempt to create a CPF object
    Then the creation should fail with a wrong type error message
