Feature: Email behavior

  Scenario: Get email address
    Given a valid email "mario_ryan_almada@ci.com.br"
    When I get the address
    Then the returned address should be "mario_ryan_almada@ci.com.br"
