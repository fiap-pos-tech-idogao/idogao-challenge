Feature: CPF Class Behaviors

  Scenario: Validating a valid formatted CPF number
    Given I have a valid formatted CPF number "148.036.300-62"
    When I validate the CPF number
    Then the validation should pass successfully

  Scenario: Validating a valid unformatted CPF number
    Given I have a valid formatted CPF number "14803630062"
    When I validate the CPF number
    Then the validation should pass successfully

  Scenario: Unformatting a valid formatted CPF number
    Given I have a valid formatted CPF number "148.036.300-62"
    When I unformat the CPF number
    Then it should be unformatted as "14803630062"

  Scenario: Formatting a valid unformatted CPF number
    Given I have a valid unformatted CPF number "148.036.300-62"
    When I format the CPF number
    Then it should be formatted as "148.036.300-62"
