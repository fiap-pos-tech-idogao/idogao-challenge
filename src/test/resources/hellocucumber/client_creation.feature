Feature: Create client object

  Scenario: Create a valid Client object
    Given I have a valid CPF object with number "374.700.778-39"
    And a valid name "Mário Ryan Henry Almada"
    And a valid email object "mario_ryan_almada@ci.com.br"
    When I attempt to create a Client object
    Then the Client object should be created successfully
    
  Scenario: Create a Client object with a invalid CPf
    Given I have a invalid CPF object with number "374.700.778-38"
    And a valid name "Mário Ryan Henry Almada"
    And a valid email object "mario_ryan_almada@ci.com.br"
    When I attempt to create a Client object
    Then the Client object should fail with a message "Invalid client: Client CPF must be valid"
    
  Scenario: Create a Client object without name
    Given I have a valid CPF object with number "374.700.778-39"
    And a valid email object "mario_ryan_almada@ci.com.br"
    When I attempt to create a Client object
    Then the Client object should fail with a message "Invalid client: Client name cannot be null"
    
  Scenario: Create a Client object with a blank name
    Given I have a valid CPF object with number "374.700.778-39"
    And a valid name " "
    And a valid email object "mario_ryan_almada@ci.com.br"
    When I attempt to create a Client object
    Then the Client object should fail with a message "Invalid client: Client name cannot be empty or blank"
    
  Scenario: Create a Client object with a empty name
    Given I have a valid CPF object with number "374.700.778-39"
    And a valid name ""
    And a valid email object "mario_ryan_almada@ci.com.br"
    When I attempt to create a Client object
    Then the Client object should fail with a message "Invalid client: Client name cannot be empty or blank"
    
  Scenario: Create a Client object with a invalid email
    Given I have a valid CPF object with number "374.700.778-39"
    And a valid name "Mário Ryan Henry Almada"
    And a invalid email object "mario_ryan_almadaci.com.br"
    When I attempt to create a Client object
    Then the Client object should fail with a message "Invalid client: Client e-mail must be valid"