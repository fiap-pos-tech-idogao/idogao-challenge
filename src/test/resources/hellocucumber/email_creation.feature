Feature: Email Creation

  Scenario: Creating a new Email object
    Given I have a valid email address "mario_ryan_almada@ci.com.br"
    When I create a Email object
    Then the email creation should be created successfully

  Scenario: Create a new invalid Email object
    Given I have a invalid email address "mario_ryan_almadaci.com.br"
    When I create a Email object
    Then the email creation should fail with a message "Email must be valid"
    
    Scenario: Create a new invalid Email object without domain
    Given I have a invalid email address "mario_ryan_almada@"
    When I create a Email object
    Then the email creation should fail with a message "Email must be valid"

  Scenario: Creating a Email object without a email
    Given I don't have a email
    When I create a Email object
    Then the email creation should fail with a message "Email cannot be null"

  Scenario: Create a Email object with a empty email
    Given I have a invalid email address ""
    When I create a Email object
    Then the email creation fail with a message "Email cannot be empty or blank"

  Scenario: Create a Email object with a white email
    Given I have a invalid email address " "
    When I create a Email object
    Then the email creation fail with a message "Email cannot be empty or blank"
