# iDogz App | iDogao Challenge

## Overview

Welcome to the *iDogao Challenge* or simply **iDogz App**! This project is a monolithic application designed with modern software development practices such as Hexagonal Architecture and Domain-driven Design (DDD). The application is built in Java (Spring Boot) with a PostgreSQL database and containerized in Docker.

## Additional Resources

> To teachers

For more detailed information, refer to the additional resources provided. Enjoy building and exploring the application!

- **Event Storming (DDD) Board:** [Miro Board](https://miro.com/app/board/uXjVKVogDfI=/)
- **iDogz Architecture Wiki:** [Notion Wiki](https://idogz.notion.site/2fb7933dc7da4a978375010be4f26a3e?v=964be9e5745240828dfc12fb8bc3ad3a&pvs=74)

## Technologies Used

### Architectural Patterns

- **Hexagonal Architecture**
- **DDD:** Domain-driven design (Event Storming, Ubiquitous Language)

### Application

- **Java:** 22
- **Spring Boot:** 3.2.5
- **Spring Cloud:** 2023.0.1
- **Maven:** 3.9.6

### Database

- **PostgreSQL:** 16

### Containerization

- **Docker Compose:** ≥ 3.4

### API Documentation

- **[Swagger](http://localhost:8080/swagger-ui/index.html)** *(Considering the default port)*

## Docker Setup

The application is containerized with Docker, using Docker Compose to manage the containers. It includes two containers: one for the application and one for the database.

## Prerequisites

- **Docker & Docker Compose**
- Default ports must be available:
  - **Database:** 5432
  - **Application:** 8080

## Getting Started

1. **Clone the Repository**

    ```bash
    git clone https://gitlab.com/fiap-pos-tech-idogao/idogao-challenge.git
    ```

2. **Navigate to the Project Directory**

    ```bash
    cd idogao-challenge
    ```

3. **Build and Start the Containers**

    ```bash
    docker-compose up
    ```

4. **Access Swagger for API Details**

    Open your **[browser](http://localhost:8080/swagger-ui/index.html)** *(Considering the default port)* and navigate to:

    ```bash
    http://localhost:8080/swagger-ui/index.html # Considering the default port
    ```

    This will provide detailed documentation of the available APIs.

5. **Stop the Containers**

    ```bash
    docker-compose down
    ```

## Additional Information

### General

Some information is previously entered through the application and/or in the database, in the `data/` folder see our `init.sql` and `store_procedures/` folder.

### Category

The Categories (or *"Categorias" in PT-BR*) are previously inserted into the database and their Category internal ID's (*idCategory*) *generally* are:

> in PT-BR

- *idCategory:* 1 = *Lanche*
- *idCategory:* 2 = *Acompanhamento*
- *idCategory:* 3 = *Bebida*
- *idCategory:* 4 = *Sobremesa*

### Phases

The Steps (*Status Order*, or *"Etapas" in PT-BR*) are previously inserted into the database and their internal Step IDs (*idStatus*) *generally* are:

- *idStatus:* 1 = Awaiting Payment (*Aguardando Pagamento*)
- *idStatus:* 2 = Received (*Recebido*)
- *idStatus:* 3 = In Preparation (*Em Preparação*)
- *idStatus:* 4 = Ready (*Pronto*)
- *idStatus:* 5 = Completed (*Finalizado*)
