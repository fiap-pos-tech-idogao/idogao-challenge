CREATE TABLE tbClient
(
 idClient SERIAL PRIMARY KEY,
 name VARCHAR(200) NOT NULL,
 email VARCHAR(100) NOT NULL,
 cpf CHAR(11) NOT NULL,
 UNIQUE (cpf)
);

CREATE TABLE tbOrder
(
 idOrder SERIAL PRIMARY KEY,
 ticketOrder SERIAL NOT NULL,
 dateOrder TIMESTAMP NOT NULL,
 totalValueOrder FLOAT NOT NULL,
 idStatus INT NOT NULL,
 idClient INT NOT NULL,
 notesOrder VARCHAR(100)
);

CREATE TABLE tbStatusOrder
(
 idStatus SERIAL PRIMARY KEY,
 description VARCHAR(100) NOT NULL,
 UNIQUE (description)
);

CREATE TABLE tbOrderProduct
(
 idOrder INT NOT NULL,
 idProduct INT NOT NULL,
 quantity INT NOT NULL,
 notes VARCHAR(200),
 PRIMARY KEY(idOrder, idProduct)
);

CREATE TABLE tbProduct
(
 idProduct SERIAL PRIMARY KEY,
 name VARCHAR(100) NOT NULL,
 priceProduct FLOAT NOT NULL,
 description VARCHAR(200) NOT NULL,
 idCategory INT NOT NULL,
 urlImage VARCHAR(500),
 active BOOLEAN NOT NULL DEFAULT True,
 UNIQUE (name,description)
);

CREATE TABLE tbCategory
(
 idCategory SERIAL PRIMARY KEY,
 nameCategory VARCHAR(50) NOT NULL,
 UNIQUE (nameCategory)
);

ALTER TABLE tbOrder ADD FOREIGN KEY(idStatus) REFERENCES tbStatusOrder (idStatus);
ALTER TABLE tbOrder ADD FOREIGN KEY(idClient) REFERENCES tbClient (idClient);
ALTER TABLE tbOrderProduct ADD FOREIGN KEY(idOrder) REFERENCES tbOrder (idOrder);
ALTER TABLE tbOrderProduct ADD FOREIGN KEY(idProduct) REFERENCES tbProduct (idProduct);
ALTER TABLE tbProduct ADD FOREIGN KEY(idCategory) REFERENCES tbCategory (idCategory);

-- Criação de Categorias
INSERT INTO tbCategory (nameCategory) VALUES('Lanche'),('Acompanhamento'),('Bebida'),('Sobremesa');
--Criação de Status do Pedido
INSERT INTO tbStatusOrder (description) VALUES('Aguardando Pagamento'),('Recebido'),('Em Preparação'),('Pronto'),('Finalizado');

-- Criação dos Itens da Categoria Lanches
INSERT INTO tbProduct (name, priceProduct, description, idCategory, urlImage,active) VALUES ('Cachorro-quente Clássico', 7.99, 'Pão de hot dog,Salsicha tradicional,Ketchup e mostarda',1,'s3://idogz/products/snacks/cachorro_quente_classico.png',True);
INSERT INTO tbProduct (name, priceProduct, description, idCategory, urlImage,active) VALUES ('Cachorro-quente Especial', 9.99, 'Pão de hot dog,Salsicha de frango,Maionese verde e batata palha',1,'s3://idogz/products/snacks/cachorro_quente_especial.png',True);
INSERT INTO tbProduct (name, priceProduct, description, idCategory, urlImage,active) VALUES ('Cachorro-quente Vegano', 788.99, 'Pão de hot dog integral,Salsicha vegetal,Maionese vegana e cebola caramelizada',1,'s3://idogz/products/snacks/cachorro_quente_vegano.png',True);

-- Criação dos Itens da Categoria Acompanhamentos
INSERT INTO tbProduct(name, priceProduct, description, idCategory, urlImage,active) VALUES ('Batata Frita', 8.99, 'Pequena', 2,'s3://idogz/products/side_dishes/batata_frita.png',True);
INSERT INTO tbProduct(name, priceProduct, description, idCategory, urlImage,active) VALUES ('Onion Rings', 9.99, 'Pequena', 2, 's3://idogz/products/side_dishes/onio_rings.png',True);
INSERT INTO tbProduct(name, priceProduct, description, idCategory, urlImage,active) VALUES ('Nuggets de Frango', 10.99, '6 unidades', 2, 's3://idogz/products/side_dishes/nuggets_frango.png',True);

-- Criação dos Itens da Categoria Bebidas
INSERT INTO tbProduct(name, priceProduct, description, idCategory, urlImage,active) VALUES ('Coca-Cola', 5.99, 'Coca Cola lata 350ml',3,'s3://idogz/products/drinks/coca_cola.png',True);
INSERT INTO tbProduct(name, priceProduct, description, idCategory, urlImage,active) VALUES ('Suco Natural de Laranja', 6.99, 'Copo de 500ml',3,'s3://idogz/products/drinks/suco_natural_laranja.png',True);
INSERT INTO tbProduct(name, priceProduct, description, idCategory, urlImage,active) VALUES ('Água Mineral', 39.99, 'Garrafa de 500ml',3,'s3://idogz/products/drinks/agua_mineral.png',True);

-- Criação dos Itens da Categoria Sobremesas
INSERT INTO tbProduct(name, priceProduct, description, idCategory, urlImage,active) VALUES ('Sundae de Chocolate', 12.99, 'Copo de 500ml', 4,'s3://idogz/products/desserts/sundae_chocolate.png',True);
INSERT INTO tbProduct(name, priceProduct, description, idCategory, urlImage,active) VALUES ('Cheesecake de Morango', 14.99, 'Unidade', 4,'s3://idogz/products/desserts/cheesecake_morango.png',True);
INSERT INTO tbProduct(name, priceProduct, description, idCategory, urlImage,active) VALUES ('Brownie com Sorvete', 11.99, 'Unidade', 4,'s3://idogz/products/desserts/brownie_sorvete.png',True);

--Criação de um cliente Dummy
INSERT INTO tbClient(name, email, cpf) VALUES ('iDoguetz', 'iDoguetz@idoguetz.com', '73868683917');