--DROP FUNCTION public.get_category_by_id(integer);

CREATE OR REPLACE FUNCTION public.get_category_by_id(_categoryid integer)
 RETURNS TABLE(idcategory integer, namecategory character varying)
 LANGUAGE plpgsql
AS $function$
BEGIN
    RETURN QUERY
    SELECT t.idcategory, t.namecategory
    FROM tbCategory t
    WHERE t.idcategory = _categoryid;
END;
$function$
;