CREATE OR REPLACE FUNCTION create_order_product(
    _idorder INTEGER, 
    _idproduct INTEGER, 
    _quantity INTEGER, 
    _notes VARCHAR(200))
RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO tborderproduct (idorder, idproduct, quantity, notes)
    VALUES (_idorder, _idproduct, _quantity, _notes);
END;
$$;