CREATE OR REPLACE FUNCTION create_order(
    _dateorder DATE,
    _totalvalueorder DOUBLE PRECISION, -- Ajuste conforme necessário baseado no tipo real da coluna
    _idstatus INTEGER,
    _idclient INTEGER,
    _notesorder VARCHAR(100)
)
RETURNS TABLE(idorder INTEGER, ticketorder INTEGER)
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN QUERY
    INSERT INTO tborder (idorder, ticketorder, dateorder, totalvalueorder, idstatus, idclient, notesorder)
    VALUES (NEXTVAL(pg_get_serial_sequence('tborder','idorder')), NEXTVAL(pg_get_serial_sequence('tborder','ticketorder')), _dateorder, _totalvalueorder, _idstatus, _idclient, _notesorder)
    RETURNING tborder.idorder, tborder.ticketorder;
END;
$$;