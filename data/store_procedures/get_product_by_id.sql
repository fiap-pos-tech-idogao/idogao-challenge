-- DROP FUNCTION public.get_product_by_id(integer);

CREATE OR REPLACE FUNCTION public.get_product_by_id(_productid integer)
 RETURNS TABLE(idproduct integer, idcategory integer, name character varying, priceproduct float8, description character varying, active bool, urlimage character varying)
 LANGUAGE plpgsql
AS $function$
BEGIN
    RETURN QUERY
    SELECT t.idproduct, t.idcategory, t.name, t.priceproduct, t.description, t.active, t.urlimage
    FROM tbProduct t
    WHERE t.idproduct = _productid;
END;
$function$
;